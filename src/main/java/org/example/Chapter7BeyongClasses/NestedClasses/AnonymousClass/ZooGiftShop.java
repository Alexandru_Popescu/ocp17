package org.example.Chapter7BeyongClasses.NestedClasses.AnonymousClass;

public class ZooGiftShop {
    abstract class SaleTodayOnly{
        abstract int dollarsOff();
    }
    public int admission(int basePrice){
        SaleTodayOnly sale = new SaleTodayOnly() {
            int dollarsOff() {
                return 3;
            }
        };
        return basePrice-sale.dollarsOff();
    }

}
