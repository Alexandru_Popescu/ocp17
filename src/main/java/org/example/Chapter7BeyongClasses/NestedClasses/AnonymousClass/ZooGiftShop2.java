package org.example.Chapter7BeyongClasses.NestedClasses.AnonymousClass;

public class ZooGiftShop2 {

    interface SaleTodayOnly {
        int dollarsOff();
    }

    public int admission(int basePrice) {
        SaleTodayOnly saleTodayOnly = new SaleTodayOnly() {
            @Override
            public int dollarsOff() {
                return 3;
            }


        };
        return basePrice;
    }
}
