package org.example.Chapter7BeyongClasses.NestedClasses.InnerClass;

public class A {

    private int x = 10;

    class B {
        private int x = 20;

        class C {
            private int y = 30;
            public void allTheX() {
                System.out.println(x);
//                System.out.println(this.x);
                System.out.println(B.this.x);
                System.out.println(A.this.x);
            }

        }



    }
    public static void main(String[] args) {
        A a = new A();
        A.B ab = a.new B();
        A.B.C ac = ab.new C();
        ac.allTheX();
    }
}