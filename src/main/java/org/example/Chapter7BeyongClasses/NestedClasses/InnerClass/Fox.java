package org.example.Chapter7BeyongClasses.NestedClasses.InnerClass;

public class Fox {
    private class Den {}

    public void goHome() {
        new Den();
    }

    public static void visitFriend() {
//    new Den();
        Fox fox = new Fox();
        Den den = fox.new Den();
        Den d = new Fox().new Den();
    }


    public class Squirrel{
        public void visitFox(){
            new Den();
        }
    }

}
