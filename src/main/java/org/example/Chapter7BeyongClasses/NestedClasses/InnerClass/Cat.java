package org.example.Chapter7BeyongClasses.NestedClasses.InnerClass;

public class Cat {
    protected class Name {
        String name = "Tom";
    }

    protected void check() {
        var name = new Name();
    }


    public static void main(String[] args) {
//        var name = new Name();

        var myCat = new Cat();
        var name = myCat.new Name();

    }

}
