package org.example.Chapter7BeyongClasses.EncapsulationAndRecord.Record.Sintaxa;

public record Cat(int age, String name) {

    public static void main(String[] args) {
        var cat =new Cat(1,"Tome");
        var cat2 =new Cat(cat.age(), cat.name());
    }

}
