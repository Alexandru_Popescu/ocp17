package org.example.Chapter7BeyongClasses.EncapsulationAndRecord.Record.Constructor;

public record Cat(int age, String name) {
    public Cat(String firstName, String lastName) {
        this(1, firstName + " " + lastName);
    }


}
