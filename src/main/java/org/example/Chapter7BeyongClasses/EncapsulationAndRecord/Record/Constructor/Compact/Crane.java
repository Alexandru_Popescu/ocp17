package org.example.Chapter7BeyongClasses.EncapsulationAndRecord.Record.Constructor.Compact;

public record Crane(int numberEggs, String name) {


    public Crane {
        if (name == null || name.length() < 1) {
            throw new IllegalArgumentException();
        }
        name = name.substring(0, 1).toUpperCase();
        name.substring(1).toLowerCase();

    }

    public static void main(String[] args) {
        var crane = new Crane(1, null);
        System.out.println(crane);

    }

}
