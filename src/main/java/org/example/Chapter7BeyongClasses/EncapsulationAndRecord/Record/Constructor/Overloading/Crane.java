package org.example.Chapter7BeyongClasses.EncapsulationAndRecord.Record.Constructor.Overloading;

public record Crane(int numberEggs, String name) {

    public Crane(String firstName, String lastName){
        this(0,firstName +" " + "");
    }

    public Crane(int age){
        this(0,"a");
    }

}
