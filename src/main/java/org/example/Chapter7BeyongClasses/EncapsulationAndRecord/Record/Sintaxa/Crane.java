package org.example.Chapter7BeyongClasses.EncapsulationAndRecord.Record.Sintaxa;

public   record Crane(int numberEggs, String name) {

    public static void main(String[] args) {
        var crane = new Crane(1,"A");
        var crane1 = new Crane(crane.numberEggs(), "SA");

    }


}
