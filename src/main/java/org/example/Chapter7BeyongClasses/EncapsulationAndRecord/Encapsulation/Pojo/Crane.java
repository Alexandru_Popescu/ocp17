package org.example.Chapter7BeyongClasses.EncapsulationAndRecord.Encapsulation.Pojo;

public class Crane {
    int numberEggs;
    String name;

    public Crane(int numberEggs, String name){
        this.numberEggs=numberEggs;
        this.name=name;
    }

}
