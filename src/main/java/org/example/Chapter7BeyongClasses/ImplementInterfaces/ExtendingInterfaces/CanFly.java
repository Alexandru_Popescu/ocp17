package org.example.Chapter7BeyongClasses.ImplementInterfaces.ExtendingInterfaces;

public interface CanFly {
    void flap();
}
