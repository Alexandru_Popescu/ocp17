package org.example.Chapter7BeyongClasses.ImplementInterfaces.DeclaringConcreteInterfaceMethods.Default.Definition;

public interface IsColdBlooded {
    boolean hasScales();
    default double getTemperature(){
        return 19;
    }

}
