package org.example.Chapter7BeyongClasses.ImplementInterfaces.DeclaringConcreteInterfaceMethods.Default.Definition;

import org.example.Chapter7BeyongClasses.ImplementInterfaces.DeclaringConcreteInterfaceMethods.Default.Definition.IsColdBlooded;

public class Snake implements IsColdBlooded {
    public boolean hasScales(){
        return true;
    }

}
