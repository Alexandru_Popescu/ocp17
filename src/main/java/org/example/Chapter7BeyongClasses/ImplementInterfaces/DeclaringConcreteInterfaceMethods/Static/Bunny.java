package org.example.Chapter7BeyongClasses.ImplementInterfaces.DeclaringConcreteInterfaceMethods.Static;

public class Bunny implements Hope{

    public void printDetails(){
        System.out.println(Hope.getJumpHeight());
    }

    public static void main(String[] args) {
        Hope hope =new Bunny();

    }
}
