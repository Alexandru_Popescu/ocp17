package org.example.Chapter7BeyongClasses.ImplementInterfaces.DeclaringConcreteInterfaceMethods.CallMembers;

public interface A {

    int m();

    default int n() {
        m();

        return 0;
    }

    private int s() {
        m();
        return 2;
    }

    private static int c() {
        return 3;
    }

    private static void checkWantYouCanCall() {
        c();


    }

    private void check() {
        m();
        s();
        c();
        checkWantYouCanCall();
    }
}
