package org.example.Chapter7BeyongClasses.ImplementInterfaces.DeclaringConcreteInterfaceMethods.Static;

public interface Hope {
    static int getJumpHeight(){
        return 4;
    }

}
