package org.example.Chapter7BeyongClasses.ImplementInterfaces.DeclaringAndUsingInterfaces;

/*
 no final
 no instantiated
 implicit abstract in top level type, public abstract in methods, public final static in field
 */


public  interface Climb {
    Number getSpeed(int age);

}
