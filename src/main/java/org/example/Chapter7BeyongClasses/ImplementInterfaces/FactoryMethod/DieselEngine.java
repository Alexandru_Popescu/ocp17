package org.example.Chapter7BeyongClasses.ImplementInterfaces.FactoryMethod;

public class DieselEngine implements Engine {

    @Override
    public void run() {
        System.out.println("Diesel");
    }
}
