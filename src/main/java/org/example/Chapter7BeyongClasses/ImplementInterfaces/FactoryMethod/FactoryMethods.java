package org.example.Chapter7BeyongClasses.ImplementInterfaces.FactoryMethod;

public final class FactoryMethods {

    private FactoryMethods() {


    }

    public static Engine build(String type) {


        return switch (type) {
            case "electric" -> new ElectricEngine();
            case "diesel" -> new DieselEngine();
            default -> throw new IllegalArgumentException();
        };

    }


}
