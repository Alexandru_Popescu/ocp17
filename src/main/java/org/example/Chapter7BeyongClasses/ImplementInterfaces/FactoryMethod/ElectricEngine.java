package org.example.Chapter7BeyongClasses.ImplementInterfaces.FactoryMethod;

public class ElectricEngine implements Engine{

    @Override
    public void run() {
        System.out.println("Electric");
    }
}
