package org.example.Chapter7BeyongClasses.ImplementInterfaces.FactoryMethod;

public class Car {

    private Engine engine;
    private FactoryMethods factoryMethods;

    public Car() {
        this.engine = Engine.build("diesel");
    }

}
