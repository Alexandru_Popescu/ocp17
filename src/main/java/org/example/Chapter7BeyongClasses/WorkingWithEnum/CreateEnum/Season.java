package org.example.Chapter7BeyongClasses.WorkingWithEnum.CreateEnum;
// we cannot extend enum


public enum Season {
    WINTER, SPRING, SUMMER, FALL;

    public static void main(String[] args) {
        var s = Season.SPRING;
        System.out.println(Season.SUMMER);
        System.out.println(s == Season.SPRING);


        // values(), ordinal() name()
        for (Season season : Season.values()) {
            System.out.println(season);
        }
        Season[] a = Season.values();
        for (int i = 0; i<=a.length-1; i++){
            System.out.println(a[i]);
        }

        //valuesOf

        Season season= Season.valueOf("SPRING");
//        Season season2= Season.valueOf("sPRING");
        System.out.println(season);
//        System.out.println(season2);

        String text = "SPRING";
       Season season1= Season.valueOf(text);
        System.out.println(season1);




    }
}
