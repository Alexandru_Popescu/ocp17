package org.example.Chapter7BeyongClasses.WorkingWithEnum.EnumsInSwitch;

public enum Season2 {

    WINTER, SPRING, SUMMER, FALL;

    public static void main(String[] args) {

        Season2 s = Season2.SUMMER;

        switch (s) {
            case WINTER -> System.out.println("Smth");
            case FALL -> System.out.println("ok");
            case SUMMER -> System.out.println("oks");
            default -> System.out.println("sds");
//            case Season2.SPRING -> System.out.println("sdsdsd");

        }


    }

}
