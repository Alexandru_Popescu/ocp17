package org.example.Chapter7BeyongClasses.WorkingWithEnum.AddingConstructorFieldMethods;

public enum Season3 {
    WINTER("Low"), SPRING("Medium"), SUMMER("High"), FALL("Medium");

    private final String expectedVisitors;

    private Season3(String expectedVisitors) {
        this.expectedVisitors = expectedVisitors;
    }

    public void printExpectedVisitors() {
        System.out.println(expectedVisitors);
    }

    public static void main(String[] args) {
        Season3 season3 = Season3.SUMMER;
        season3.printExpectedVisitors();
    }


}
