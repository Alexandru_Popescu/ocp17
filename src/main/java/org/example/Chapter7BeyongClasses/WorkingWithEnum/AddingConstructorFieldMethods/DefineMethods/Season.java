package org.example.Chapter7BeyongClasses.WorkingWithEnum.AddingConstructorFieldMethods.DefineMethods;

public enum Season {

    Winter {
        public String getHours() {
            return "3";
        }
    },
    SPRING {
        public String getHours() {
            return "5";
        }
    },
    FALL {
        public String getHours() {
            return "5";
        }

        int a() {
            return 5;
        }



    };

    abstract String getHours();

     int a() {
       return 2;
    }

}
