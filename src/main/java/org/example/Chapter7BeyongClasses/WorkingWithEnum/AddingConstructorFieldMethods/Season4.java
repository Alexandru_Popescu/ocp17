package org.example.Chapter7BeyongClasses.WorkingWithEnum.AddingConstructorFieldMethods;

public enum Season4 {
    WINTER("COLD"),SPRING("HOT");

    private Season4(String s){
        System.out.println("Season4.values()");
    }

    public static void main(String[] args) {
        var a = Season4.values();
        System.out.println("smth");
        var b = Season4.values();
        System.out.println(Season4.SPRING);
    }

}
