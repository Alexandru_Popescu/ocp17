package org.example.Chapter7BeyongClasses.Polymorphism.MethodOverride;

public class Penguin {
    public int getHeight(){
        return 3;
    }
    public void printInfo(){
        System.out.println(this.getHeight());
    }

}
