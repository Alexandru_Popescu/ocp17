package org.example.Chapter7BeyongClasses.Polymorphism.MethodOverride;

public class EmperorPenguin extends Penguin{

    public int getHeight() {
        return 8;
    }

    public void printInfo() {
        System.out.println(super.getHeight());
    }

    public static void main(String[] args) {
        new EmperorPenguin().printInfo();
    }
}
