package org.example.Chapter7BeyongClasses.Polymorphism.Check2;

public class Main {

    public static void main(String[] args) {
        A a = new B();
        System.out.println(a.x);
        B b = new B();
        System.out.println(b.x);
        System.out.println(b.y);
        B b1 = (B)a;
        System.out.println(b1.y);
        System.out.println(b1.x);
        C c = new C();
     double d = 2.3;
     int e = (int) d;
        System.out.println(e);
        d = e;
        System.out.println(d);

    }
}
