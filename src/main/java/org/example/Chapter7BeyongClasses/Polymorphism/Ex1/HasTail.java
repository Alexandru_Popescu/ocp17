package org.example.Chapter7BeyongClasses.Polymorphism.Ex1;

public interface HasTail {

    public abstract boolean isTailStriped();

}
