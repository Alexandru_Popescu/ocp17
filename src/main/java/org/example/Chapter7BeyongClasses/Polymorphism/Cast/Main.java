package org.example.Chapter7BeyongClasses.Polymorphism.Cast;

public class Main {
    public static void main(String[] args) {
        Dog b = new Dog();

        //implicit cast
        Animal a = new Dog();
        //explicit Cast
        Dog b1 = (Dog) a;
        //runtimeException
        Animal a1 = new Cat();
//      B b2 = (B)a1;

         //compiler error
        Cat c = new Cat();
//        Cat c1 = (Cat)a;
//        B b2 = (B)c1;


    }
}
