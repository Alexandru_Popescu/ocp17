package org.example.Chapter7BeyongClasses.Polymorphism.Check;

public class Main {

    public static void main(String[] args) {

        //inheritance
        A a = new A();
        System.out.println(a.x);
        a.m();
        B b = new B();
        System.out.println(b.x = " 34");
        b.n();
        System.out.println(b.x);
        b.m();
        C c = new C();
        System.out.println(c.a);
        c.check();
        c.n();
        System.out.println(c.x);
        c.m();
        //polymorphism
        A a1 = new B();
        System.out.println(a1.x);
        a1.m();
        B b2 = (B) a1;



//        B b3 = (B)a;
    }

}
