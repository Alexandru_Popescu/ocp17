package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Instance;

public interface StringStar {
    boolean beginningCheck(String prefix);

}
