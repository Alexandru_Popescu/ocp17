package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Instance;

public class Main {
    public static void main(String[] args) {
        var str = "Zoo";

        StringStar stringStar = (s) -> str.startsWith(s);
        StringStar stringStar1 = str::startsWith;
        StringStar stringStar2 = String::isEmpty;
        System.out.println(stringStar.beginningCheck("A"));
        System.out.println(stringStar1.beginningCheck(str));

        StringChecker stringChecker = () -> str.isEmpty();
        StringChecker stringChecker1 = str::isEmpty;
        System.out.println(stringStar2.beginningCheck("a"));
    }
}
