package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Instance;

public interface StringChecker {
    boolean check();

}
