package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.TheLastCheck;

public class MethodReferenceTester {

    public static void m() {
        System.out.println("static");
    }

    public void wrongSignature(int a) {
        System.out.println("w");
    }



    public int test() {
        System.out.println("inside test method");
        return 5;
    }

    public boolean dummy() {
        System.out.println("inside dummy method");
        return true;
    }

    public static void main(String[] args) {
        Playable p = () -> System.out.println("Lambda");
        p.play();
        MethodReferenceTester methodReferenceTester = new MethodReferenceTester();
        Playable p1 = methodReferenceTester::dummy;
        p1.play();
        Playable p2 = methodReferenceTester::test;
        p2.play();
        Playable p3 = MethodReferenceTester::m;
        p3.play();
//        Playable p4 = methodReferenceTester::wrongSignature;
    }
}
