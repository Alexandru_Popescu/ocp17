package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.TheLastCheck;
@FunctionalInterface
public interface Playable {
    public void play();
}
