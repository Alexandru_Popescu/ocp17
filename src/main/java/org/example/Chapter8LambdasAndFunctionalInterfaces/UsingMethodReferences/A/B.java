package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.A;

public class B {
    public static void main(String[] args) {
        A a = () -> System.out.println("Hello ");
        A b = System.out::println;
        a.m();
        b.m();
        A a1 = ()-> {
            System.out.println("fdf");
        };

    }
}
