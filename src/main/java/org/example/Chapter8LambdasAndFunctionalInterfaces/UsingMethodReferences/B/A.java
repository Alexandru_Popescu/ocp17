package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.B;

public interface A {

    boolean check(String prefix);
}
