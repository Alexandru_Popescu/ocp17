package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Static;

public class Main {
    public static void main(String[] args) {


        Converter converter = (s) -> Math.round(s);
        Converter converter1 = Math::round;
        System.out.println(converter.round(123.3));
        System.out.println(converter1.round(2334.6));



      //example without lambda and method references
        Check check = new Check();
        System.out.println(Math.round(check.round( 35.8 )));
        System.out.println(Math.round(check.round( 37.6 )));
        System.out.println(Math.round(check.round( 34.4 )));

    }
}