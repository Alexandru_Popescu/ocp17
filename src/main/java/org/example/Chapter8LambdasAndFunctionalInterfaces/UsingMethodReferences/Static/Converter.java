package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Static;

public interface Converter {
   long round(double num);
}
