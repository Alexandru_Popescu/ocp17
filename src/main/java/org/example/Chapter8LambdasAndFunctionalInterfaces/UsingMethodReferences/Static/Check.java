package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Static;

public class Check implements Converter {

    public long round(double num) {

        return Math.round(num);
    }
}
