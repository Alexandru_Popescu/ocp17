package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Example;

public interface LearnSpeak {
    void speak(String sound);
}
