package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Example;

public class Duckling {
    public static void makeSound(String sound) {
//        LearnSpeak learner = (s) -> System.out.println(s);
        LearnSpeak learner = System.out::println;
        DuckHelper.teacher(sound, learner);
    }
}
