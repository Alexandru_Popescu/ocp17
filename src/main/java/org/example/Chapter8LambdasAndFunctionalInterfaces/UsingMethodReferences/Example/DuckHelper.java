package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Example;

public class DuckHelper {
    public static void teacher(String name, LearnSpeak trainer){
        trainer.speak(name);
    }

}
