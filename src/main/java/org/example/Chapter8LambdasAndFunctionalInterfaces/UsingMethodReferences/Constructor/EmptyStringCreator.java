package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Constructor;

interface EmptyStringCreator {
    String create();

}
