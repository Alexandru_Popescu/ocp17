package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Constructor;

public class Main {
    public static void main(String[] args) {
        EmptyStringCreator emptyStringCreator = ()-> new String();
        EmptyStringCreator emptyStringCreator1 = String::new;
        System.out.println(emptyStringCreator1.create().isEmpty());
        System.out.println(emptyStringCreator.create());
    }
}
