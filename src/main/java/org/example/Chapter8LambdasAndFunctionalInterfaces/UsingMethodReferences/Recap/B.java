package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Recap;

public interface B {
    boolean check(String prefix);
}
