package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Recap;

public interface A {

    String check(String prefix);

}
