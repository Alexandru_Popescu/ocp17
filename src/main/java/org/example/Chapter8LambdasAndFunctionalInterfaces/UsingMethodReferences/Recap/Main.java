package org.example.Chapter8LambdasAndFunctionalInterfaces.UsingMethodReferences.Recap;

public class Main {
    public static void main(String[] args) {
        A a = (s)-> "Hello";
        System.out.println(a.check(""));
        A a1 = String::toLowerCase;
        System.out.println(a1.check("HELLO"));
        A A2 = String::toUpperCase;



    }
}
