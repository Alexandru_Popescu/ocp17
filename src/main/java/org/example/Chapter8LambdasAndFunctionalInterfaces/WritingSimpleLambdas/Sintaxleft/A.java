package org.example.Chapter8LambdasAndFunctionalInterfaces.WritingSimpleLambdas.Sintaxleft;

public class A {
    public static void main(String[] args) {

        NoParameters noParameters = () -> System.out.println();    // cannot be skip ()
        OneParameters oneParameters = x -> System.out.println();    // can be skip()
        OneParameters oneParameters1 = (x) -> System.out.println();    // can be skip()
        OneParameters oneParameters2 = (int x) -> System.out.println();    // cannot be skip ()
        OneParameters oneParameters3 = (var x) -> System.out.println();    // cannot be skip ()
        OneParameters oneParameters4 = s->{};
        TwoParameters twoParameters = (x, u) -> System.out.println(""); // cannot be skip ()
        TwoParameters twoParameters1 = (int x, String u) -> System.out.println(""); // cannot be skipe () and the type should be the same

    }
}
