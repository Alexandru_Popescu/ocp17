package org.example.Chapter8LambdasAndFunctionalInterfaces.WritingSimpleLambdas.SintaxRight;

public class Main {
    public static void main(String[] args) {
        Z z = () -> 2;
        Z z1 = () -> m(2);
        Z z2 = () -> {
            return 4;
        };    // return only in{}


    }

    static int m(int a) {
        return a;
    }
}
