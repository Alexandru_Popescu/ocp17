package org.example.Chapter8LambdasAndFunctionalInterfaces.WritingSimpleLambdas.ExWithoutLambdas;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        var animals = new ArrayList<Animal>();
        animals.add(new Animal("fish", false, true));
        animals.add(new Animal("kangaroo", true, false));
        animals.add(new Animal("rabbit", true, false));
        animals.add(new Animal("turtle", false, true));
//        print(animals, new CheckIfHopper());
//        print(animals, new CheckIfSwim());

    print(animals, a ->a.canHop());
    print(animals, a ->a.canSwim());

    }


    private static void print(List<Animal> animals, CheckTrait checkTrait) {

        for (Animal animal : animals) {
            if (checkTrait.test(animal))
                System.out.println(animal + " ");
        }
        System.out.println();

    }
}
