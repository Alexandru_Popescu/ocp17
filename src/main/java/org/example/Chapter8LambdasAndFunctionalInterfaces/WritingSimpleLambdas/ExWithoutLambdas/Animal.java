package org.example.Chapter8LambdasAndFunctionalInterfaces.WritingSimpleLambdas.ExWithoutLambdas;

public record Animal(String species,boolean canHop, boolean canSwim) {
}
