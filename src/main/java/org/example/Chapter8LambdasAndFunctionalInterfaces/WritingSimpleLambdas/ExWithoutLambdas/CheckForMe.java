package org.example.Chapter8LambdasAndFunctionalInterfaces.WritingSimpleLambdas.ExWithoutLambdas;

import java.util.ArrayList;
import java.util.List;

public class CheckForMe {
    public static void main(String[] args) {
        var animals = new ArrayList<Animal>();
        animals.add(new Animal("a",true,true));
        animals.add(new Animal("a",true,false));
        animals.add(new Animal("a",true,false));
        animals.add(new Animal("a",true,true));
        animals.add(new Animal("a",true,true));
//        check(animals,new CheckIfSwim());
    }

    private static void check(List<Animal>animals, CheckTrait checkTrait){
        for (Animal animal : animals){
            if (checkTrait.test(animal)){
                System.out.println(animal +" ");
            }
        }
        System.out.println();
    }


}
