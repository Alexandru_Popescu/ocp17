package org.example.Chapter8LambdasAndFunctionalInterfaces.WritingSimpleLambdas.ExWithoutLambdas;

public interface CheckTrait {
    boolean test (Animal a);

}
