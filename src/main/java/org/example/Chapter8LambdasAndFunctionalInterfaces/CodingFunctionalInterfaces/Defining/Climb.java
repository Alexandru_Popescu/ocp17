package org.example.Chapter8LambdasAndFunctionalInterfaces.CodingFunctionalInterfaces.Defining;

@FunctionalInterface
public interface Climb {
    void reach();

    default void fall() {
    }

    static int getBackUp() {
        return 100;
    }

    private static boolean check() {
        return true;
    }

}
