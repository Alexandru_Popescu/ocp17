package org.example.Chapter8LambdasAndFunctionalInterfaces.CodingFunctionalInterfaces.Defining;

@FunctionalInterface
public interface Sprint {
    public void sprint(int speed);


}
