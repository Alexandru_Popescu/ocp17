package org.example.Chapter8LambdasAndFunctionalInterfaces.DefaultMethods;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {
    // and
    // or
    //negate

    public static void main(String[] args) {

        Predicate<String> egg = s -> s.contains("egg");
        Predicate<String> brown = s -> s.contains("brown");
//        Predicate<String> brownsEggs = s-> s.contains("egg") && s.contains("brown");
//        Predicate<String> otherEggs = s-> s.contains("egg") && !s.contains("brown");
        Predicate<String> brownEggs = egg.and(brown);
        Predicate<String> otherEggs = egg.and(brown.negate());

        Consumer<String> c1 = x -> System.out.print(x+ " 1");
        Consumer<String> c2 = x -> System.out.print(x+ " 1");
        Consumer<String> combined = c1.andThen(c2);
        combined.accept("A");


        Function<Integer, Integer> f1 = x -> x+1;
        Function<Integer, Integer> f2 = x -> x*2;
        Function<Integer, Integer> compos = f1.compose(f2);
        System.out.println(compos.apply(3));


    }


}
