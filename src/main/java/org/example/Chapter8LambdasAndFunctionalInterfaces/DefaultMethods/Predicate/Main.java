package org.example.Chapter8LambdasAndFunctionalInterfaces.DefaultMethods.Predicate;

import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        Consumer<String> s1 = (s) -> System.out.print("a" + s);
        Consumer<String> s2 = (s) -> System.out.print(s + "b");
        Consumer<String> comb = s1.andThen(s2);
        comb.accept("c");



    }
}
