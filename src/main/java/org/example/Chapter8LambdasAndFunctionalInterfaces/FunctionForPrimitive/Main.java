package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionForPrimitive;

import java.util.function.DoubleUnaryOperator;

public class Main {
    public static void main(String[] args) {
        DoubleUnaryOperator d1 = (s) -> s;
        System.out.println(d1.applyAsDouble(2l));
    }

}
