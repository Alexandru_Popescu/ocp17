package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionForPrimitive.DoubleIntLongSupplier;

import java.util.function.DoubleFunction;
import java.util.function.DoubleSupplier;
import java.util.function.IntSupplier;
import java.util.function.LongSupplier;

public class Main {
    public static void main(String[] args) {
        DoubleSupplier s1 = ()-> 3.4;
        IntSupplier i1 = () -> 1;
        LongSupplier l1 = ()->4;
        DoubleFunction<String> d1 = (s) -> "" + s + s;
        System.out.println(d1.apply(2.4));
    }
}
