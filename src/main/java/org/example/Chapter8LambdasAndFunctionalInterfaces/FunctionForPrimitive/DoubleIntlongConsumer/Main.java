package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionForPrimitive.DoubleIntlongConsumer;

import java.util.function.DoubleConsumer;
import java.util.function.DoubleFunction;

public class Main {
    public static void main(String[] args) {

        DoubleConsumer d1 = (s) -> System.out.println(s);
        d1.accept(2.3);
        DoubleFunction<Double> d2 = (s)-> 3.5;

    }


}

