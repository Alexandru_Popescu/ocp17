package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionalIntefaces.Recap;

import java.util.List;
import java.util.Random;
import java.util.function.*;

public class Main {

    public static void main(String[] args) {

        //ex1: Supplier
        Random r = new Random();
        Supplier<String> s1 = () -> "Hello";
        System.out.println(s1.get());
        Supplier<Integer> s2 = () -> r.nextInt(30);
        int a = s2.get();
        System.out.println(a);

        System.out.println("----------------");

        Consumer<String> c1 = (s) -> System.out.println(s);
        c1.accept("Hello");
        List<String> c2 = List.of("A", "B");
        c2.forEach((x) -> System.out.print(x));

        BiConsumer<String, String> c3 = (c, d) -> System.out.println(c + " " + d);
        c3.accept("a", "b");

        System.out.println("----------------");

        Function<String, Integer> f1 = (x) -> x.length();
        int b = f1.apply("Hello");
        System.out.println(b);

        System.out.println("----------------");

        BiPredicate<Integer, Integer> p1 = (z, y) -> z + y > 7;
        boolean c = p1.test(1, 2);
        System.out.println(c);
    }


}
