package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionalIntefaces.BiConsumer;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;

public class Main {
    public static void main(String[] args) {
        BiConsumer<Integer, Integer> s1 = (x, y) -> System.out.println(x + " " + y);
        int x = 4;
        int y = 5;

        s1.accept(x, y);

        List<Integer> list = Arrays.asList(1, 2, 3, 4);
        list.forEach(z -> System.out.print(z));


    }
}