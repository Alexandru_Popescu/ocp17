package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionalIntefaces.UnaryOperator;

import java.util.function.UnaryOperator;

public class Main {

    public static void main(String[] args) {
        UnaryOperator<Integer> u1 = (x) -> x;
       int a = u1.apply(5);
        System.out.println(a);
    }
}
