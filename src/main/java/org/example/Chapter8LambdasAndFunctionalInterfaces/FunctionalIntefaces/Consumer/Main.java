package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionalIntefaces.Consumer;

import java.util.function.Consumer;

public class Main {
    public static void main(String[] args) {
        Consumer<String> s1  = (s)-> System.out.println(s);
        Consumer<String> s2  = System.out::println;
        s1.accept("Hello");
        s2.accept("Hello");
    }
}
