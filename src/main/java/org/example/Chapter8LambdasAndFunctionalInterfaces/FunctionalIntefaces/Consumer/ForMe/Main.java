package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionalIntefaces.Consumer.ForMe;

public class Main {
    public static void main(String[] args) {
        C c = (x) -> System.out.println(x);
        c.accept("Hello");
    }
}
