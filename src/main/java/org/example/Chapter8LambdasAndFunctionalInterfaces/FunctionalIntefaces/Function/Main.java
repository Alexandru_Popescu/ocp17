package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionalIntefaces.Function;

import java.util.function.BiFunction;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {
        Function<String, Integer> f1 = (x) -> x.length();
        int a = f1.apply("Hello");
        System.out.println(a);
        Function<String, Integer> f2 = String::length;
        int b = f2.apply("aaa");
        System.out.println(b);
        BiFunction<String, String, String> f3 = (z, y) -> z + y;
        String result = f3.apply("AAA", "BBB");
        System.out.println(result);
        BiFunction<String, String, String> f4 = String::concat;
        String res = f4.apply("A", "b");
        System.out.println(res);
        Function<String, String > f5 = (x) -> x.toLowerCase();
        String res1 = f5.apply("ABCD");
        System.out.println(res1);
        BiFunction<String, String, Integer> f6 = (z, y) -> (z +y).length();
        int res2 =f6.apply("ab","Bd");
        System.out.println(res2);
    }
}
