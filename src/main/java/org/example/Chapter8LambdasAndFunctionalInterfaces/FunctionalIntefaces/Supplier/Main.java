package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionalIntefaces.Supplier;

import java.util.Random;
import java.util.function.Supplier;

public class Main {

    public static void main(String[] args) {
        Random random = new Random();

        Supplier<Integer> a = () -> random.nextInt(23);
        Integer b = a.get();
        System.out.println(b);
    }


}
