package org.example.Chapter8LambdasAndFunctionalInterfaces.FunctionalIntefaces.Predicate;

import java.util.function.BiPredicate;
import java.util.function.Predicate;

public class Main {
    public static void main(String[] args) {
        Predicate<Integer> p1 = (x) -> x > 5;
        boolean a = p1.test(3);
        System.out.println(a);

        BiPredicate<Integer, Integer> p2 = (z, y) -> z.equals(y) ;
        boolean b =p2.test(2,2);
        System.out.println(b);
    }
}
