package org.example.Chapter4CoreAPIs.Equality;

public class Ex1 {
    public static void main(String[] args) {

        var one = new StringBuilder();    // one =a
        var two = new StringBuilder();    // two = ""
        var three= one.append("a");  //three = "a"
        System.out.println(one==two);   //false
        System.out.println(one==three);  //true
        System.out.println(two==three); // false

        Boolean result = one.equals(three);
                 //In StringBuilder we don't have toStringMethod. If we call equals(), checks the reference

    }
}
