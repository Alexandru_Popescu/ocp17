package org.example.Chapter4CoreAPIs.Equality;

public class Ex2 {
    public static void main(String[] args) {

        var one = new String();
        var two  = new String();
        System.out.println(one==two);
        var three = "Alex";
        var four  = "Alex";
        System.out.println(three==four);

    }
}
