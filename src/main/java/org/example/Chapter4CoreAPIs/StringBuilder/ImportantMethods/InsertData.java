package org.example.Capitolul4CoreAPIs.StringBuilder.ImportantMethods;

public class InsertData {
    public static void main(String[] args) {
        var sb= new StringBuilder("animals");
                              //   0123456
        sb.insert(7, "-");   //animals-
        sb.insert(0, "-");   //-animals-
        sb.insert(4, "-");   //-anim-als-
        System.out.println(sb);
    }
}
