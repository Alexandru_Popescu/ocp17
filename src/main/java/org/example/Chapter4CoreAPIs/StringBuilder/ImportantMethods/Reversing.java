package org.example.Capitolul4CoreAPIs.StringBuilder.ImportantMethods;

public class Reversing {
    public static void main(String[] args) {
        var a = new StringBuilder("abc");
        a.reverse();
        System.out.println(a);
    }
}
