package org.example.Capitolul4CoreAPIs.StringBuilder.ImportantMethods;

public class Delete {
    public static void main(String[] args) {
        var sb = new StringBuilder("abcdef");
                               //   012345
                               //   0345
        sb.delete(1,3);      //adef
//        sb.deleteCharAt(5);  excepion

   var a = new StringBuilder("Andrei");
        //                    012345

        a.delete(2,4);        //    0145   anei
        System.out.println(a);
        a.delete(1,1000);
        System.out.println(a);
    }
}
