package org.example.Capitolul4CoreAPIs.StringBuilder.ImportantMethods;

public class SubStringAndIndexOfAndLength {
    public static void main(String[] args) {

        var sb = new StringBuilder("animals");
        String sub = sb.substring(sb.indexOf("a"),sb.indexOf("al"));     //anim
        int len = sb.length();       // 7
        char ch = sb.charAt(6);      // l
        System.out.println(sub + " " + len + " " + ch);


    }
}
