package org.example.Capitolul4CoreAPIs.StringBuilder.ImportantMethods;

public class Appending {
    public static void main(String[] args) {
        var sb = new StringBuilder().append(1).append('c');
        sb.append("-").append(true);
        System.out.println(sb);
    }
}
