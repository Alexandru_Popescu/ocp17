package org.example.Capitolul4CoreAPIs.StringBuilder.ImportantMethods;

public class ReplacingPortions {
    public static void main(String[] args) {

        var builder = new StringBuilder("pigeon dirty");
                                //       01234567891011
                                //       012    7891011
        builder.replace(3,6,"sty");   //pigsty dirty
        System.out.println(builder);

        var b = new StringBuilder("pigeon dirty");
        b.replace(3,22,"a");
        System.out.println(b);


    }
}
