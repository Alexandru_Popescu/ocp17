package org.example.Capitolul4CoreAPIs.StringBuilder;

public class ExampleDiffBetweenStringAndSB {

    public static void main(String[] args) {

        String name="";
        for (char current ='a'; current<='z'; current++){
            name+=current;
        }
        System.out.println(name);
        System.out.println(name.length());   // we have 27 object ("" + 26 letters)

        StringBuilder name1 = new StringBuilder();
        for (char current ='a'; current <='z';current++){
            name1.append(current);
        }
        System.out.println(name1);
    }
}
