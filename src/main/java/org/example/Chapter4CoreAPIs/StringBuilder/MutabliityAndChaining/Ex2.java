package org.example.Capitolul4CoreAPIs.StringBuilder.MutabliityAndChaining;

public class Ex2 {
    public static void main(String[] args) {
        StringBuilder a = new StringBuilder("abc");   //abcde
        StringBuilder b = a.append("de");          //abcde
        b = b.append("f").append("g");             //abcdefg
        System.out.println("a=" + a);               //abcdefg
        System.out.println("b=" + b);                //abcdefg


    }
}
