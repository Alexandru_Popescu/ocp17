package org.example.Capitolul4CoreAPIs.StringBuilder.MutabliityAndChaining;

public class DiffStringVsBuilder2 {
    public static void main(String[] args) {
      String name = "Alex   ";   //first Object -
      String result = name.toLowerCase().stripTrailing().replace('a','A').substring(1);
//                        2 Object        3               4                                  5
        System.out.println(name);
        System.out.println(result);

//StringBuilder

        StringBuilder sb = new StringBuilder("Alex");  // firstObject
        StringBuilder a = sb.append(" Popescu");       //firstObject with 2 var reff   Alex Popescu
        sb.append(" Daniel");                           // Alex Popescu Daniel
        a.append(" este");
        sb.append(" programator");

        System.out.println(a);


    }
}
