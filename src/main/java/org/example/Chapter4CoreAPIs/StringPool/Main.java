package org.example.Capitolul4CoreAPIs.StringPool;

public class Main {
    public static void main(String[] args) {

        String name ="Alex";
        String name1 ="Alex";
        System.out.println(name==name1);
        System.out.println(name.equals(name1));
        String name2 = new String("A");
        String name3 = new String("A");
        System.out.println(name2==name3);
        System.out.println(name2.equals(name3));
        StringBuilder name4 = new StringBuilder("A");
        StringBuilder name5 = new StringBuilder("A");
        System.out.println(name4==name5);
        System.out.println(name4.equals(name5));

    }
}
