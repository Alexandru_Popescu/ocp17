package org.example.Capitolul4CoreAPIs.StringPool;

public class Ex1 {
    public static void main(String[] args) {
        var x = "Hello World";
        var y =" Hello World".trim();
        System.out.println(x == y);

        var singleString = "hello world";
        var concat="hello ";
        concat += "world";
        System.out.println(singleString==concat);


        var a = "Hello World";
        var b = new String("Hello World");
        System.out.println(a==b);


        var c ="Hi";
        var d= new String("Hi").intern();
        System.out.println(c == d);

    }
}
