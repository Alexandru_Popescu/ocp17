package org.example.Capitolul4CoreAPIs.StringPool;

public class Ex3 {
    public static void main(String[] args) {

        String name ="Alex";
        name="Ion";   // name with two objects
        String text = "Hello";
        String text1 ="Hello";   // text si text1 2 variabile catre acelasi obiect
        System.out.println(text == text1);
        String a = new String("Hi");
        String b = new String("Hi");
        System.out.println(a == b);  // because we create the objects at runtime


    }
}
