package org.example.Capitolul4CoreAPIs.StringPool;

public class Ex2 {
    public static void main(String[] args) {

        var first ="rat" +1;          //rat1
        var second ="r"+"a"+"t"+1;    //  rat1
        var third ="r"+"a"+"t"+ new String("1");
        System.out.println(first == second);  //true
        System.out.println(first == second.intern()); // false
        System.out.println(first == third);
        System.out.println(first == third.intern());

    }
}
