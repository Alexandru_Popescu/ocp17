package org.example.Capitolul4CoreAPIs.Math;

public class Rounding {
    public static void main(String[] args) {
        long low = Math.round(123.45);
        int low2 = Math.round(4.0f);
        long high = Math.round(123.5);
        int fromFloat = Math.round(123.45f);
        System.out.println(low);
        System.out.println(high);
        System.out.println(fromFloat);
    }
}
