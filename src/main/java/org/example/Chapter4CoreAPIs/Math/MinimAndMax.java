package org.example.Capitolul4CoreAPIs.Math;

public class MinimAndMax {
    public static void main(String[] args) {
        int first = Math.max(3, 7);
        int second = Math.min(7, -9);
        double third = Math.min(3.4,4.0);
        float fourth = Math.min(3.4f,4.0f);
        System.out.println(first);
        System.out.println(second);
        System.out.println(third);
        System.out.println(fourth);

    }
}
