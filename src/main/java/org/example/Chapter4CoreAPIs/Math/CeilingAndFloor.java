package org.example.Capitolul4CoreAPIs.Math;

public class CeilingAndFloor {
    public static void main(String[] args) {
        double a = Math.ceil(3);
        double b = Math.ceil(3.1f);
        System.out.println(a);
        System.out.println(b);
        double c = Math.floor(2.1);
        double d = Math.floor(2.7);
        System.out.println(c);
        System.out.println(d);
    }
}
