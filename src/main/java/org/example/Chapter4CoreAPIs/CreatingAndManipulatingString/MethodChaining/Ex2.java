package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.MethodChaining;

public class Ex2 {
    public static void main(String[] args) {
        String a = "abc";
        String b = a.toUpperCase();  //ABC
        b = b.replace("B", "2").replace('C', '3');  //A23
        System.out.println("a=" + a);
        System.out.println("b=" + b);


    }
}
