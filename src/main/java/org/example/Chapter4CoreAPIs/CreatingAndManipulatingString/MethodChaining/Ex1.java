package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.MethodChaining;

public class Ex1 {
    public static void main(String[] args) {
        var star = "AniMal   ";
        var trimmed = star.trim();   //AniMal
        var lowercase= trimmed.toLowerCase();  // animal
        var result = lowercase.replace('a','A');   // AnimAl
        System.out.println(result);
        String result2 = "AniMal   ".trim().toLowerCase().replace('a','A');
        System.out.println(result2);
    }
}
