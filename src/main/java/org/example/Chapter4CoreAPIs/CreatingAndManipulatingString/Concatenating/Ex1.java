package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.Concatenating;

public class Ex1 {
    public static void main(String[] args) {

        /*
        Rules:
      If both operands are numeric, + means numeric addition
      If either operand is a String, + means concatenation
      The expression is evaluated left to right
            */

        System.out.println(1 + 2);   //3
        System.out.println("a" + " b");   // ab
        System.out.println("a" + " b" + 3);   // ab3
        System.out.println("a" + " b" + 3);   // ab3
        System.out.println(1 + 2 + "c");   // 3c
        System.out.println(1 + 2 + "c");   // 3c
        System.out.println("c" + 1 + 2);   // c12
        System.out.println("c" + null);   //c null
        System.out.println("c" + 4 + 5);   //c45
        System.out.println(4 + 5 + "c");   //9c


        // Ex2

        int three = 3;
        String four = "4";
        System.out.println(1 + 2 + three + four); //64

        // Ex3
        var s = "1";
        s += "2";
        s += 3;
        System.out.println(s);


    }
}