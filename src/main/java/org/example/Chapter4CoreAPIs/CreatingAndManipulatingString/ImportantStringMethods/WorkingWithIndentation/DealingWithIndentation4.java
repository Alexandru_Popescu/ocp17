package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods.WorkingWithIndentation;

public class DealingWithIndentation4 {
    public static void main(String[] args) {

   var name = """
           A
            B 
           C
            D""";

        System.out.println(name);
        System.out.println(name.length());
        System.out.println(name.indent(1));
        System.out.println(name.stripIndent());

        var text = " a\n"
                   + "b\n"
                   + "   c";
        System.out.println(text);
        System.out.println(text.stripIndent());

    }
}
