package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods;

public class CharAt {
    public static void main(String[] args) {
        var name ="animals";
        System.out.println(name.charAt(0));
        System.out.println(name.charAt(6));
        // System.out.println(name.charAt(7)); --> Exception
    }
}
