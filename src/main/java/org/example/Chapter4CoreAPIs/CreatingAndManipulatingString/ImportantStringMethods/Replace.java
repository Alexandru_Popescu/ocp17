package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods;

public class Replace {
    public static void main(String[] args) {
        String name = "Alex";
        System.out.println("aVDFD".replace('a','C'));
        System.out.println("aVDFD".replace("a","C"));
        System.out.println(name.replace("Ale", "ALE"));
    }
}
