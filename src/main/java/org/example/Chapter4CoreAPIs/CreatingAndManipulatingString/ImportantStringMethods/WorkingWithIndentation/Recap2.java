package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods.WorkingWithIndentation;

public class Recap2 {
    public static void main(String[] args) {
        var concat = "  a\n"
                   + "   b\n"
                   + "  c";
        System.out.println(concat.length());
        System.out.println(concat.stripIndent().length());

    }
}
