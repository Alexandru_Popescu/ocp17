package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods;

public class Substring {
    public static void main(String[] args) {

        var name = "animals";
        System.out.println(name.substring(3)); //mals
        System.out.println(name.substring(name.indexOf('m')));
        System.out.println(name.substring(3,4));
        System.out.println(name.substring(3,7));

        System.out.println("=====");

        System.out.println(name.length());
        System.out.println(name.substring(0, 7));
        System.out.println(name.substring(3, 3));
//        System.out.println(name.substring(3, 8));
//        System.out.println(name.substring(3, 2));


    }
}
