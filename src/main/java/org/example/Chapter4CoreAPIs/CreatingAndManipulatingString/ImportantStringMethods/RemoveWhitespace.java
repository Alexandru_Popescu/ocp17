package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods;

public class RemoveWhitespace {
    public static void main(String[] args) {

//        System.out.println("abc".strip());
//        System.out.println("\t   a b c\n".strip() );

        String text = " abc\t ";
        System.out.println(text.length());   //6
        System.out.println(text.trim().length());   //3
        System.out.println(text.strip().length());   //3
        System.out.println(text.stripLeading().length());   //5
        System.out.println(text.stripTrailing().length());   //4

    }
}
