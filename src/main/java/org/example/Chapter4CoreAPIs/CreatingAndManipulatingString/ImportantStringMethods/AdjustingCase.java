package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods;

public class AdjustingCase {
    public static void main(String[] args) {
        var name = "Alex";
        System.out.println(name.toUpperCase()); // Atentie!!! Alex is still there
        System.out.println("ABG34334F".toLowerCase());
    }
}
