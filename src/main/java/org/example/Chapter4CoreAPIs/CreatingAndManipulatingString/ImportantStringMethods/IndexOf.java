package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods;

public class IndexOf {
    public static void main(String[] args) {

        var name = "animals";
        System.out.println(name.indexOf('n'));  // 1
        System.out.println(name.indexOf("al"));  // 4
        System.out.println(name.indexOf('a', 4));  // 4
        System.out.println(name.indexOf("al",5));  //-1
        System.out.println(name.indexOf(2));  //-1
    }
}
