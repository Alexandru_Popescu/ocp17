package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods.WorkingWithIndentation;

public class DealingWithIndentation2 {
    public static void main(String[] args) {
        var block = """
                a 
                 b
                c""";
        System.out.println(block);
        System.out.println(block.indent(1));


        var concat = " a\n"
                + "  b\n"
                + " c";
        System.out.println(concat);
        System.out.println(concat.indent(-1));


    }
}
