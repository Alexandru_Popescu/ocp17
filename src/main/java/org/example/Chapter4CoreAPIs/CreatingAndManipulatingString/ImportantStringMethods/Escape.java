package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods;

public class Escape {
    public static void main(String[] args) {
        var str = "1\t2";
        System.out.println(str);
        System.out.println(str.translateEscapes());
    }
}
