package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods;

public class Equality {
    public static void main(String[] args) {
        System.out.println("abc".equals("abc"));
        System.out.println("abc".equals("ABC"));
        System.out.println("abc".equalsIgnoreCase("ABC"));
    }
}
