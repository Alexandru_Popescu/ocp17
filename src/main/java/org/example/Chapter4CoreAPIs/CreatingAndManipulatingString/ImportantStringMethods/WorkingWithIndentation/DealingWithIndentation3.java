package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods.WorkingWithIndentation;

public class DealingWithIndentation3 {
    public static void main(String[] args) {
        System.out.println("hello".indent(2));
//        System.out.println("Alex");
//
//
//        System.out.println("===");
//
//
//        System.out.println(" hello");
//        System.out.println(" hello".stripIndent());

   var name = """
           A
            B 
           C
            D""";

        System.out.println(name);
        System.out.println(name.length());
        System.out.println(name.indent(1).length());


    }
}
