package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods;

public class startWithAndEndsWithsAndContains {
    public static void main(String[] args) {
        System.out.println("abc".startsWith("a"));
        System.out.println("abc".endsWith("c"));
        System.out.println("abc".contains("d"));
    }
}
