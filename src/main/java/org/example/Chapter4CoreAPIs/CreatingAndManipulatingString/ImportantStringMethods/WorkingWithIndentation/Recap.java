package org.example.Capitolul4CoreAPIs.CreatingAndManipulatingString.ImportantStringMethods.WorkingWithIndentation;

public class Recap {
    public static void main(String[] args) {

        var blocks = """
                a
                 b
                c""";
    var concat = " a\n"
               + "  b\n"
               + " c";
        System.out.println(blocks.length());   //6
        System.out.println(concat.length());   //9
        System.out.println(blocks.indent(1).length());   //10
        System.out.println(concat.indent(-1).length());  //7
        System.out.println(concat.indent(-4).length());  // 6
        System.out.println(concat.stripIndent().length());  // 6


    }
}