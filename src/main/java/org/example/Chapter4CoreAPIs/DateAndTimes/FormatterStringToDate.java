package org.example.Chapter4CoreAPIs.DateAndTimes;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class FormatterStringToDate {
    public static void main(String[] args) {
        String date= "2022-10-31";
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US);
        LocalDate localDate = LocalDate.parse(date,df);
        System.out.println(localDate);


    }
}
