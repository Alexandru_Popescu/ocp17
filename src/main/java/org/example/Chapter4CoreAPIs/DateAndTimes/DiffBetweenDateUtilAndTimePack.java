package org.example.Chapter4CoreAPIs.DateAndTimes;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DiffBetweenDateUtilAndTimePack {
    public static void main(String[] args) {
        /*
          Legacy!!!  Util.Date is mutable -->
          has date and Time -->first diff
         */

        Date d1 = new Date();
        System.out.println(d1);
        System.out.println(d1.toInstant());

        System.out.println("====");
        Calendar calendar = Calendar.getInstance();
        Calendar calendar1 = new GregorianCalendar();
        calendar.set(1011,11,2);    // LocalDate
        Date d2 = calendar.getTime();
        System.out.println(d2);

    }
}
