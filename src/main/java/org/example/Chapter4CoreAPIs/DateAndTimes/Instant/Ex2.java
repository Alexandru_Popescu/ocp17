package org.example.Chapter4CoreAPIs.DateAndTimes.Instant;

import java.time.Instant;
import java.time.ZonedDateTime;

public class Ex2 {
    public static void main(String[] args) {

        Instant instant = Instant.now();
        System.out.println(instant);
        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        System.out.println(zonedDateTime);

    }
}
