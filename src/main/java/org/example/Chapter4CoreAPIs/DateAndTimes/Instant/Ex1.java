package org.example.Chapter4CoreAPIs.DateAndTimes.Instant;

import java.time.*;

public class Ex1 {
    public static void main(String[] args) {

        var localDate= LocalDate.of(2022,3,23);
        var time = LocalTime.of(22,23);
        var zone = ZoneId.systemDefault();
        var zonedDateTime = ZonedDateTime.of(localDate,time,zone);
        var instant = zonedDateTime.toInstant();
        System.out.println(zonedDateTime);
        System.out.println(instant);
    }
}
