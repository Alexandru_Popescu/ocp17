package org.example.Capitolul4CoreAPIs.DateAndTimes.DaylightSavingTime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Ex5November {
    public static void main(String[] args) {
        var date = LocalDate.of(2020,10,25);
        var time = LocalTime.of(1,30,22);
        var zonedDateTime= ZonedDateTime.of(date,time, ZoneId.systemDefault());
        System.out.println(zonedDateTime);
        System.out.println(zonedDateTime.getOffset());
        zonedDateTime=zonedDateTime.plusHours(3);
        System.out.println(zonedDateTime);
        System.out.println(zonedDateTime.getOffset());

    }
}
