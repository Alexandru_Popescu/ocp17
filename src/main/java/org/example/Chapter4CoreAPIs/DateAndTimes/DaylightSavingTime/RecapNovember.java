package org.example.Chapter4CoreAPIs.DateAndTimes.DaylightSavingTime;

import java.time.*;
import java.time.ZonedDateTime;

public class RecapNovember {
    public static void main(String[] args) {

        var date = LocalDate.of(2023, Month.OCTOBER,29);
        var time = LocalTime.of(1,0);
        var zonedDateTime = ZonedDateTime.of(date,time, ZoneId.systemDefault());
        System.out.println(zonedDateTime);
        zonedDateTime=zonedDateTime.plusHours(3);
        System.out.println(zonedDateTime);


    }
}
