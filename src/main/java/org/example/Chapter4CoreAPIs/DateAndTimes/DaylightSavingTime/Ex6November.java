package org.example.Capitolul4CoreAPIs.DateAndTimes.DaylightSavingTime;

import java.time.*;
import java.time.ZonedDateTime;

public class Ex6November {
    public static void main(String[] args) {

        var date = LocalDate.of(2022, Month.NOVEMBER,6);
        var time = LocalTime.of(1,30);
        var zonedDateTime = ZonedDateTime.of(date,time, ZoneId.of("US/Eastern"));
        System.out.println(zonedDateTime);
        zonedDateTime.plusHours(1);
        System.out.println(zonedDateTime);

        var date2 = LocalDate.of(2022,Month.MARCH,13);
        var time2= LocalTime.of(2,40);
        var zonedDateTime2= ZonedDateTime.of(date2,time2,ZoneId.of("US/Eastern"));
        System.out.println(zonedDateTime2);

    }
}
