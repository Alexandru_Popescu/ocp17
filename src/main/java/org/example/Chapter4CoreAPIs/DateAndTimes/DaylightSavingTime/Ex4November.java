package org.example.Capitolul4CoreAPIs.DateAndTimes.DaylightSavingTime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Ex4November {

    public static void main(String[] args) {
        var localDate = LocalDate.of(2022, Month.NOVEMBER,6);
        var localTime = LocalTime.of(1,30);
        var zoneId = ZoneId.of("US/Eastern");
        var zonedDateTime = ZonedDateTime.of(localDate,localTime,zoneId);
        System.out.println(zonedDateTime);
        zonedDateTime = zonedDateTime.plusHours(1);
        System.out.println(zonedDateTime);
    }
}
