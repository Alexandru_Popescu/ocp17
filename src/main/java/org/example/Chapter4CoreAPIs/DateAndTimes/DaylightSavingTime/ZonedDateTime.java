package org.example.Capitolul4CoreAPIs.DateAndTimes.DaylightSvingTime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;

public class ZonedDateTime {
    public static void main(String[] args) {

//        ZoneId.getAvailableZoneIds().forEach(System.out::println);
        var zonedDateTimes = java.time.ZonedDateTime.of(
                LocalDate.of(2020, 10, 25),
                LocalTime.of(1, 0), ZoneId.systemDefault());
        System.out.println(zonedDateTimes);
        zonedDateTimes = zonedDateTimes.plusHours(3);
        System.out.println(zonedDateTimes);
    }
}
