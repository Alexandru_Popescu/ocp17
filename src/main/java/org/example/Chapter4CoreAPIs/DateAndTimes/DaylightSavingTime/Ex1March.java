package org.example.Chapter4CoreAPIs.DateAndTimes.DaylightSavingTime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Ex1March {
    public static void main(String[] args) {
        var date = LocalDate.of(2022, Month.MARCH,13);
        var time = LocalTime.of(1,30);
        var zone = ZoneId.of("US/Eastern");
        var zoneTime= ZonedDateTime.of(date,time,zone);
        System.out.println(zoneTime);
        System.out.println(zoneTime.getHour());
        System.out.println(zoneTime.getOffset());
        zoneTime = zoneTime.plusHours(1);
        System.out.println(zoneTime);
        System.out.println(zoneTime.getHour());
        System.out.println(zoneTime.getOffset());
    }
}
