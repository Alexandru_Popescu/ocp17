package org.example.Chapter4CoreAPIs.DateAndTimes.DaylightSavingTime;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class Ex2March {
    public static void main(String[] args) {
        var date = LocalDate.of(2022, Month.MARCH,13);
        var time = LocalTime.of(1,10);
        var zone = ZoneId.of("US/Eastern");
        var zoneDateTime = ZonedDateTime.of(date,time,zone);
        System.out.println(zoneDateTime);
        System.out.println(zoneDateTime.getOffset());
        zoneDateTime=zoneDateTime.plusHours(1);
        System.out.println(zoneDateTime);
        System.out.println(zoneDateTime.getOffset());


    }
}
