package org.example.Chapter4CoreAPIs.DateAndTimes.DaylightSavingTime;

import java.time.*;

public class RecapRomania {
    public static void main(String[] args) {
        var date= LocalDate.of(2023, Month.MARCH,26);
        var time= LocalTime.of(3,20);
        ZonedDateTime zonedDateTime = ZonedDateTime.of(date,time,ZoneId.systemDefault());
        System.out.println(zonedDateTime);
        zonedDateTime = zonedDateTime.plusHours(1);
        System.out.println(zonedDateTime);

    }
}
