package org.example.Chapter4CoreAPIs.DateAndTimes.ManipulatingDatesAndTimes;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class ManipulatingTimes {
    public static void main(String[] args) {
        var date = LocalDate.of(2024, Month.JANUARY,20);
        var time = LocalTime.of(5,15);
        var dateTime = LocalDateTime.of(date,time);
        System.out.println(dateTime);
        dateTime = dateTime.minusDays(1);
        System.out.println(dateTime);
        dateTime= dateTime.minusHours(10);
        System.out.println(dateTime);
        dateTime = dateTime.minusSeconds(30);
        System.out.println(dateTime);


    }
}
