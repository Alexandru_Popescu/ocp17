package org.example.Chapter4CoreAPIs.DateAndTimes.ManipulatingDatesAndTimes;

import java.time.LocalDate;
import java.time.Month;

public class ManipulatingDates {
    public static void main(String[] args) {

        var date = LocalDate.of(2023, Month.SEPTEMBER, 21);
        System.out.println(date);
        date = date.plusDays(2);
        System.out.println(date);
        date = date.plusWeeks(1);
        System.out.println(date);
        date = date.plusMonths(1);
        System.out.println(date);
        date = date.plusYears(5);
        System.out.println(date);

    }
}
