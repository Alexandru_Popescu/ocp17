package org.example.Chapter4CoreAPIs.DateAndTimes.CreatingDateandTimes;

import java.time.*;
import java.time.ZonedDateTime;


public class Main {
    public static void main(String[] args) {
        System.out.println(LocalDate.now());   // 19/09/2023
        System.out.println(LocalTime.now());   // 18:11
        System.out.println(LocalDateTime.now());    //2023/09/19 // 18:12
        System.out.println(ZonedDateTime.now());
    }
}
