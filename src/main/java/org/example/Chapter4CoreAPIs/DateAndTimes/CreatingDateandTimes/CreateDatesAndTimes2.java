package org.example.Chapter4CoreAPIs.DateAndTimes.CreatingDateandTimes;

 import java.time.*;

 import java.time.LocalDate;
 import java.time.LocalTime;
 import java.time.Month;
 import java.time.ZoneId;
 import java.time.ZonedDateTime;
public class CreateDatesAndTimes2 {
    public static void main(String[] args) {
        var date1 = LocalDate.of(2023, Month.SEPTEMBER, 21);
        var time1 = LocalTime.of(6, 15);
        var dateTime1 = LocalDateTime.of(2023, Month.JANUARY, 20, 6, 15, 30);
        var zone = ZoneId.of("US/Eastern");
        var zone1 = ZonedDateTime.of(date1,time1,zone);
        System.out.println(zone1);
        var zone2 = ZonedDateTime.of (2022, 1, 20, 6, 15, 30, 200, zone);
        System.out.println(zone2);
        var zone3= ZonedDateTime.of(dateTime1,zone);
        System.out.println(zone3);

    }
}