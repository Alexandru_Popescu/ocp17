package org.example.Chapter4CoreAPIs.DateAndTimes.CreatingDateandTimes;

import java.time.*;

public class CreateDatesAndTimes {
    public static void main(String[] args) {
        var date1 = LocalDate.of(2023, Month.SEPTEMBER, 18);
        var date2 = LocalDate.of(2023, 9, 18);
        System.out.println(date1);
        System.out.println(date2);
        var time1 = LocalTime.of(8, 41, 20, 23232);
        System.out.println(time1);
        var localDateAndTime = LocalDateTime.of(2023, Month.SEPTEMBER, 18, 8, 43, 44, 343434);
        System.out.println(localDateAndTime);


    }
}
