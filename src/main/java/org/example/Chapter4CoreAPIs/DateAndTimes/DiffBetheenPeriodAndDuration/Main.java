package org.example.Chapter4CoreAPIs.DateAndTimes.DiffBetheenPeriodAndDuration;


import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

public class Main {
    public static void main(String[] args) {
        var localDate= LocalDate.of(2022, Month.MARCH,24);
        var period = Period.ofMonths(2);
        var duration = Duration.ofHours(2);
        System.out.println(localDate.plus(period));
//        System.out.println(localDate.plus(duration));
    }
}
