package org.example.Chapter4CoreAPIs.DateAndTimes.Durations;

import java.time.Duration;

public class Main {
    public static void main(String[] args) {

        var daily = Duration.ofDays(1);   //PT24H
        var hourly = Duration.ofHours(1); // PT1H
        var everyMinutes = Duration.ofMinutes(1);   // PT1M
        var everyTenSeconds = Duration.ofSeconds(10);
        var everyMilli = Duration.ofMillis(1);
        var everyNano = Duration.ofNanos(1);
        System.out.println(daily);
        System.out.println(hourly);
        System.out.println(everyMinutes);
        System.out.println(everyTenSeconds);
        System.out.println(everyMilli);
        System.out.println(everyNano);

    }
}
