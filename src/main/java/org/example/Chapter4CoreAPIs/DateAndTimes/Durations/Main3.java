package org.example.Chapter4CoreAPIs.DateAndTimes.Durations;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class Main3 {
    public static void main(String[] args) {
        var date = LocalDate.of(2022,1,20);
        var time = LocalTime.of(6,15);
        var dateTime = LocalDateTime.of(date,time);
        var duration = Duration.ofHours(23);
        System.out.println(dateTime.plus(duration));
        System.out.println(time.plus(duration));

//    //!!!!    System.out.println(date.plus(duration));
    }
}
