package org.example.Chapter4CoreAPIs.DateAndTimes.Period;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

public class Ex2WithPeriod {
    public static void main(String[] args) {
        var start = LocalDate.of(2022, Month.JANUARY, 1);
        var end = LocalDate.of(2022, Month.MARCH, 30);
        var end2 = LocalDate.of(2028, Month.MARCH, 30);
        var period = Period.ofMonths(1);
        var period2 = Period.ofYears(1);
        performAnimalEnrichment(start, end, period);
        performAnimalEnrichment(start, end2, period2);

    }


    private static void performAnimalEnrichment(LocalDate start, LocalDate end, Period period) {

        var upTo = start;
        while (upTo.isBefore(end)) {
            System.out.println("give new toy " + upTo);
            upTo = upTo.plus(period);
        }

    }


}
