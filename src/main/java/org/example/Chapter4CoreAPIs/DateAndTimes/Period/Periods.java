package org.example.Chapter4CoreAPIs.DateAndTimes.Period;

import java.time.Period;

public class Periods {
    public static void main(String[] args) {

        var annually = Period.ofYears(1);
        var quarterly = Period.ofMonths(3);
        var everyThreeWeeks = Period.ofWeeks(3);
        var everyOtherDay = Period.ofDays(2);
        var everyYearAndAWeek = Period.of(1,0,7);
        System.out.println(annually);
        System.out.println(quarterly);
        System.out.println(everyThreeWeeks);
        System.out.println(everyOtherDay);
        System.out.println(everyYearAndAWeek);
        System.out.println(java.time.Period.of(1,2,3));
        var wrong = Period.ofYears(1);
        wrong = Period.ofWeeks(1);
        System.out.println(wrong);
    }
}
