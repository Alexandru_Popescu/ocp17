package org.example.Chapter4CoreAPIs.DateAndTimes.Period;

import java.time.Period;

public class NoMethodsChainingWithPeriod {
    public static void main(String[] args) {

        var everyYearAndAWeek = Period.of(1,0,7);
        System.out.println(everyYearAndAWeek);
       var wrong = Period.ofYears(1).ofWeeks(2);
        System.out.println(wrong);
        var wrong2 = Period.ofYears(1);
        wrong2 = Period.ofWeeks(1);
        System.out.println(wrong2);
    }
}
