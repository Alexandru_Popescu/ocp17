package org.example.Chapter4CoreAPIs.DateAndTimes.Period;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;

public class Periods4 {
    public static void main(String[] args) {
        var date = LocalDate.of(2022,1,20);
        var time = LocalTime.of(6,15);
        var dateTime = LocalDateTime.of(date,time);
        var period= Period.ofMonths(1);
        System.out.println(date.plus(period));   // 22,2,20
        System.out.println(dateTime.plus(period));   // 22,2,20
//        System.out.println(time.plus(period));   // Exception
    }
}
