package org.example.Chapter4CoreAPIs.DateAndTimes;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FormatterDate {
    public static void main(String[] args) {
        LocalDateTime localDateTime = LocalDateTime.now();
        DateTimeFormatter df = DateTimeFormatter.ofPattern("HH:mm:ss dd:MM:YYYY");
        String result = df.format(localDateTime);
        System.out.println(result);
    }
}
