package org.example.Chapter4CoreAPIs.Array.UsingArray;

public class Ex1 {
    public static void main(String[] args) {
        String[]mammals = {"monkey","chimp","donkey"};
        System.out.println(mammals.length);
        System.out.println(mammals[0]);
        System.out.println(mammals[1]);
        System.out.println(mammals[2]);


        //attention
//        System.out.println(mammals.length());  //length is method, used in String

        var a = new String[6];
        System.out.println(a.length);


    }
}
