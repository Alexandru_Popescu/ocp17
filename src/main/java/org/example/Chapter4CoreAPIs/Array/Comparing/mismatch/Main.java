package org.example.Chapter4CoreAPIs.Array.Comparing.mismatch;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        //ex 1 where are equals
        int a [] = {1,2};
        int b [] = {1,2};   // -1
        System.out.println(Arrays.mismatch(a, b));

        //ex 2 not  equals --> return first index

        int c [] = {1,2};
        int d [] = {1,2,3};
        System.out.println(Arrays.mismatch(c, d));  //2

    }



}
