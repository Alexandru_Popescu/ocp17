package org.example.Chapter4CoreAPIs.Array.Comparing.compare;

import java.util.Arrays;

public class CompareArrays {
    public static void main(String[] args) {
        //Ex1  ==  --> 0
        int a[] = new int[]{1, 2};
        int b[] = new int[]{1, 2};
        System.out.println(Arrays.compare(a,b));

        //Ex2   ==  -1
        int c[] = new int[]{1, 2};
        int d[] = new int[]{1, 2,1};
        System.out.println(Arrays.compare(c,d));

        //Ex3   ==  1

        int e[] = new int[]{1, 2,1};
        int f[] = new int[]{1, 2};
        System.out.println(Arrays.compare(e,f));

        //Ex4   ==  -1
        int g[] = new int[]{1, 2};
        int h[] = new int[]{3, 2};
        System.out.println(Arrays.compare(g,h));
        //Ex5   ==   1
        int i[] = new int[]{3, 2};
        int j[] = new int[]{1, 2};
        System.out.println(Arrays.compare(i,j));
    }
}
