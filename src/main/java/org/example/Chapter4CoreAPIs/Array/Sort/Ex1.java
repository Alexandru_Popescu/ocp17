package org.example.Chapter4CoreAPIs.Array.Sort;

import java.util.Arrays;

public class Ex1 {
    public static void main(String[] args) {
        int [] a = {1,4,3,5};
        Arrays.sort(a);
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
        int [] b = {1,4,3,5};
        Arrays.sort(b);
        System.out.print(Arrays.toString(b) + " ");

    }
}
