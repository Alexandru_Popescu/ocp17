package org.example.Chapter4CoreAPIs.Array.CreatingArrayWithPrimitives;

public class CreatingArray {
    public static void main(String[] args) {

        int[] numbers = new int[3];  //default 000
        int[] numbers2 = new int[]{42, 55, 99};
        int[] numbers3 = {42, 55, 99};   //anonymous array

        //type []

        int[] a;
        int b[];
        int [] c;
        int  []d;
        int  [] e;



    }
}
