package org.example.Chapter4CoreAPIs.Array.Searching;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int [] numbers = {2,4,6,8};
        Arrays.binarySearch(numbers,2); //0
        Arrays.binarySearch(numbers,4); // 1
        Arrays.binarySearch(numbers,1); //-1
        Arrays.binarySearch(numbers,3); //-2
        Arrays.binarySearch(numbers,9); //-5
        System.out.println(Arrays.binarySearch(numbers, 0)); //-1
        System.out.println(Arrays.binarySearch(numbers, 7)); //     -4
        System.out.println(Arrays.binarySearch(numbers, 5)); //-3
    }
}
