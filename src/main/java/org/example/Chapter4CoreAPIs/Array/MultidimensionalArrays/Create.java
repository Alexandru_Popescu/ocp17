package org.example.Chapter4CoreAPIs.Array.MultidimensionalArrays;



public class Create {
    public static void main(String[] args) {

        String[][] rectangle = new String[3][2];
        rectangle[0][1] = "set";
        for (var a = 0; a < rectangle.length; a++) {
            for (var b = 0; b < rectangle[a].length; b++){
                System.out.print(rectangle[a][b] + " ");
            }
            System.out.println();
        }

    }
}
