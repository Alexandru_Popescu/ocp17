package org.example.Chapter4CoreAPIs.Array.MultidimensionalArrays;

public class Create4 {
    public static void main(String[] args) {

        int[][] myArrays = {{1, 2, 3}, {2}, {3, 5, 6}};
        for (int a = 0; a < myArrays.length; a++) {
            for (int j = 0; j < a; j++) {
                System.out.print(myArrays[j].length);
            }
            System.out.print(" ");
        }
    }
}
