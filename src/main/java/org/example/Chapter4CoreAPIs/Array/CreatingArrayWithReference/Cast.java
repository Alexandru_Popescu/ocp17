package org.example.Chapter4CoreAPIs.Array.CreatingArrayWithReference;

public class Cast {
    public static void main(String[] args) {
        String [] strings = {"stringValue"};
        Object[] objects = strings;
        String [] againString =(String[]) objects;
        //againString[0] = new StringBuilder();   wrongtype
      //objects[0] = new StringBuilder();

    }
}
