package org.example.Chapter6ClassDesign.InheritingMembers.Rules.Rule5HiddingStatic.Ex2;

public class SunBear extends Bear{
////    public void sneeze(){    --> should be static
//        System.out.println("Sun Bear sneezes quietly"); }


//    public static void hibernate(){    --> should be static non-static
//        System.out.println("Sun bear is going to sleep");}

//    protected static void laugh(){   --> wrong access modifiers 
//        System.out.println("Sun Bear is laughing"); }
//


}
