package org.example.Chapter6ClassDesign.InheritingMembers.Rules.Rule1methodSignature;

public class Kangaroo2 extends Marsupial{
    public double getAverageWeight() {
        return  getAverageWeight() + 20;   // atentie
    }

    public static void main(String[] args) {
        System.out.println(new Marsupial().getAverageWeight()); //50.00
        System.out.println(new Kangaroo2().getAverageWeight()); //70.00
    }
}
