package org.example.Chapter6ClassDesign.InheritingMembers.Rules.Rule5HiddingStatic.Ex1;

public class Panda extends Bear{
    public static void eat(){
        System.out.println("Panda is chewing");
    }

    public static void main(String[] args) {
        eat();
    }
}
