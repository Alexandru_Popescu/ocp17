package org.example.Chapter6ClassDesign.InheritingMembers.Rules.Rule5HiddingStatic.Ex2;

public class Bear {

    public static void sneeze(){
        System.out.println("Bear is sneezing");
    }

    public void hibernate(){
        System.out.println("Bear is hibernating");
    }

    public static void laugh(){
        System.out.println("Bear is laughing");
    }

}
