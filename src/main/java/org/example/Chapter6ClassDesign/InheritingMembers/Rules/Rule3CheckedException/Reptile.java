package org.example.Chapter6ClassDesign.InheritingMembers.Rules.Rule3CheckedException;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Reptile {

    public void sleep() throws IOException {}
    public void hide()  {}
    public void exitShell() throws FileNotFoundException {}
}
