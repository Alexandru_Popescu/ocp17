package org.example.Chapter6ClassDesign.InheritingMembers.Rules.Rules3;

import java.io.IOException;

public class A {

    protected  void  m() throws IOException {   }
    protected  void  n() throws IOException {   }
    protected  void  h() throws IOException {   }
    protected  void  g()  {   }


}
