package org.example.Chapter6ClassDesign.InheritingMembers.Rules.Rules3;

import java.io.FileNotFoundException;
import java.io.IOException;

public class B extends A {

    //rule 1 --> the same type
    public void m() throws IOException {
    }
// rule 2 --> avoid checked exception
    public void n() {
    }
//rule 3 --> subtype

   public void h() throws FileNotFoundException { }

    //unchecked exception
    public void g() throws ArithmeticException { }

}
