package org.example.Chapter6ClassDesign.InheritingMembers.Final;

public class Bird {
    public final boolean hasFeathers(){
        return true;
    }

    public final static  void flyAway(){}
}
