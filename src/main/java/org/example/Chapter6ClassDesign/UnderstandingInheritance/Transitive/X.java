package org.example.Chapter6ClassDesign.UnderstandingInheritance.Transitive;

public class X extends Y{
    public static void main(String[] args) {

        X x = new X();

        if (x instanceof Z){
            System.out.println("X is inherit Z ");
        }else System.out.println("Not");

    }
}
