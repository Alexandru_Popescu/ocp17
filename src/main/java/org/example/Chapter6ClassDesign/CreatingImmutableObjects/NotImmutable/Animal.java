package org.example.Chapter6ClassDesign.CreatingImmutableObjects.NotImmutable;

import java.util.ArrayList;

public final class Animal {

    private final ArrayList<String> favoriteFoods;

    public Animal(){
        this.favoriteFoods= new ArrayList<String>();
        favoriteFoods.add("Apple");
    }

    public ArrayList<String> getFavoriteFoods() {
        return favoriteFoods;
    }
}
