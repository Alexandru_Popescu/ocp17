package org.example.Chapter6ClassDesign.CreatingImmutableObjects.NotImmutable.A;

import java.util.ArrayList;

public final class Animal {

    private final ArrayList<String> favoriteFoods;

    public Animal(ArrayList<String> favoriteFoods){
        if (favoriteFoods==null ||favoriteFoods.size()==0)
            throw new RuntimeException("favoriteFoods is required");
        this.favoriteFoods= new ArrayList<>(favoriteFoods);
    }

    public int getFavoriteFoodsCount(){
        return  favoriteFoods.size();
    }
    public String getFavoriteFoodsItem(int index){
        return favoriteFoods.get(index);
    }

    public static void main(String[] args) {
        var favorite = new ArrayList<String>();
        favorite.add("apple");
        var zebra = new Animal(favorite);
        System.out.println(zebra.getFavoriteFoodsItem(0));
        favorite.clear();
        favorite.add("Chocolate");
        System.out.println(zebra.getFavoriteFoodsItem(0));
    }

}
