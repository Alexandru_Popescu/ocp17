package org.example.Chapter6ClassDesign.CreatingImmutableObjects.NotImmutable;

public class Main {
    public static void main(String[] args) {
        var zebra = new Animal();
        System.out.println(zebra.getFavoriteFoods());

        zebra.getFavoriteFoods().clear();
        zebra.getFavoriteFoods().add("Chocolate Chip Cookies");
        System.out.println(zebra.getFavoriteFoods());
    }


}
