package org.example.Chapter6ClassDesign.CreatingImmutableObjects;

import java.util.ArrayList;
import java.util.List;

public final class Animal {

    private final List<String> favoriteFoods;

    public Animal(){
        this.favoriteFoods=new ArrayList<String>();
        this.favoriteFoods.add("apple");

    }

    public int getFavoriteFoodsCount(){
        return favoriteFoods.size();
    }
    public String getFavoriteFoodsItem(int index){
        return favoriteFoods.get(index);
    }

//    public ArrayList<String> getFavoriteFoods(){
//        return new ArrayList<>(this.favoriteFoods);
//    }

    @Override
    public String toString() {
        return "Animal{" +
                "favoriteFoods=" + favoriteFoods +
                '}';
    }
//
//    public static void main(String[] args) {
//        Animal a = new Animal();
//        System.out.println(a);
//        Animal a1  = new Animal();
//        System.out.println(a1);
//        System.out.println(a.equals(a1));
//    }
}
