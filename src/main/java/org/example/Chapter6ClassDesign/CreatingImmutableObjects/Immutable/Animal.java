package org.example.Chapter6ClassDesign.CreatingImmutableObjects.Immutable;

import java.util.ArrayList;
public final class Animal {
    private final ArrayList<String> favoriteFoods;
    public Animal(){
        this.favoriteFoods= new ArrayList<String>();
        favoriteFoods.add("Apple");
    }
    public int getFavoriteFoodsCount() {
        return favoriteFoods.size();
    }
}
