package org.example.Chapter6ClassDesign.DeclaringConstructors.Super.Exemplu1;

public class Zebra extends Animal{
    public Zebra( int age){
        super(2);  // call Animal Constructor

    }
    public Zebra(){
        this(3);    // call the zebra constructor
    }
}
