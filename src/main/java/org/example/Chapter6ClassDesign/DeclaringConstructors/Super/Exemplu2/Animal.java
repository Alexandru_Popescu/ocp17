package org.example.Chapter6ClassDesign.DeclaringConstructors.Super.Exemplu2;

public class Animal {
    private int age;
    private String name;
    public Animal(int age,String name){
        super();
        this.age=age;
        this.name=name;
    }

    public Animal(int age){
        super();
        this.age=age;
        this.name=null;
    }

}
