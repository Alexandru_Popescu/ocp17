package org.example.Chapter6ClassDesign.DeclaringConstructors.Super.Exemplu1;

public class Animal {
    private int age;
    public Animal(int age){
        super();     // call Object constructor
        this.age=age;
    }
}
