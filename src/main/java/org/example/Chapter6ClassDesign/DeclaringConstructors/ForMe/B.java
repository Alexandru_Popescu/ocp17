package org.example.Chapter6ClassDesign.DeclaringConstructors.ForMe;

public class B extends C {

    int size;
    String name;

    public B(int size) {
        this.size = size;
    }
    public B(int size, String name){
        this.size=size;
        this.name=name;
        System.out.println("Parent constructor");
    }

}
