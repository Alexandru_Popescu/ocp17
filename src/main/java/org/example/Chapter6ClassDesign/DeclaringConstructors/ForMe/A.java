package org.example.Chapter6ClassDesign.DeclaringConstructors.ForMe;

public class A extends B{
    int age;


    public A() {
        super(3, "Alex");
        System.out.println("A from parent class ");
    }

    public A(int age){
        this();
        System.out.println("A overloading");
    }
}
