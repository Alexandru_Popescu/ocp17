package org.example.Chapter6ClassDesign.DeclaringConstructors.This;

public class Hamster {
    private String color;
    private int weight;


    public Hamster(int weight, String color){
        this.weight=weight;
        this.color=color;
    }

//    public Hamster(int weight){
//        this.weight=weight;
//        color="brown";
//    }

    public Hamster(int weight){
//        System.out.println("a");

        //   --> comments works
        this(weight,"blue");   //this should be always first statement
    }
}
