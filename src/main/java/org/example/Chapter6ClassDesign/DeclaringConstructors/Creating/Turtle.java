package org.example.Chapter6ClassDesign.DeclaringConstructors.Creating;

public class Turtle {

    private String name;

    public Turtle(){
        name="John Doe";
    }
    public Turtle(int age){}
    public Turtle(long age){}
    public Turtle(String newName, String ...favoriteFoods){}

}
