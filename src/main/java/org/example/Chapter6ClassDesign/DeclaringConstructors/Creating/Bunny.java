package org.example.Chapter6ClassDesign.DeclaringConstructors.Creating;

public class Bunny {

    public Bunny() {
        System.out.println("It's ok");
    }

    //    public bunny(){} --> Doesn't match the class name and doesn't have return type to be method--> compile error
    public void Bunny(int a) {
        return;
    }

//    public Bunny(var a ){}  -->cannot have var keyword


}

