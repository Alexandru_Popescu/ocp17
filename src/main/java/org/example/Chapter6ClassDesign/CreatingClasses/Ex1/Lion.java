package org.example.Chapter6ClassDesign.CreatingClasses.Ex1;

public class Lion extends Animal{
    protected void setProperties(int age, String n){
        setAge(age);
        name=n;
    }
    public void roar(){
        System.out.println(name + ", age " +getAge() + ", says!");
    }

    public static void main(String[] args) {
        var lion = new Lion();
        lion.setProperties(3, "Tiger ");
        lion.roar();
    }

}
