package org.example.Chapter6ClassDesign.CreatingClasses.Super.Ex1;

public class Crocodile extends Reptile{

   protected int speed =20;
   public int getSpeed(){
       return super.speed;
//       return speed;
   }

    public static void main(String[] args) {
        var crocodile = new Crocodile();
        System.out.println(crocodile.getSpeed());
    }
}
