package org.example.Chapter6ClassDesign.CreatingClasses.Super.Ex2;

public class Beetle extends Insect{
    protected int numberOfLegs =6;
    short age =3;

    public void printData(){
        System.out.println(this.label);   // buggy
        System.out.println(super.label);  //  buggy
        System.out.println(this.age);    // 3
//        System.out.println(super.age);    // compileError --> only parent class member
        System.out.println(numberOfLegs);   //6
    }

    public static void main(String[] args) {
        new Beetle().printData();
    }

}
