package org.example.Chapter6ClassDesign.CreatingClasses.This.Role;

public class Flamingo2 {

    private String color = null;

    public void setColor(String color) {

         this.color = color;
    }

    public static void main(String[] args) {
        var f = new Flamingo2();
        f.setColor("Pink");
        System.out.println(f.color);


    }
}
