package org.example.Chapter6ClassDesign.CreatingClasses.This.Role;

public class Flamingo {

    private String color = null;

    public void setColor(String color) {

        color = color;
    }

    public static void main(String[] args) {
        var f = new Flamingo();
        f.setColor("Pink");
        System.out.println(f.color);   // Java uses the most granular scope


    }
}
