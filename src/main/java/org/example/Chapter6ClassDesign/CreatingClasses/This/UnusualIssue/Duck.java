package org.example.Chapter6ClassDesign.CreatingClasses.This.UnusualIssue;

public class Duck {
    private String color;
    private int height;
    private int length;

    public void setData(int length, int theHeight){
        length =this.length;    //backwards  !Wrong! --> instance member is 0 and is assigned to the parameter
        height = theHeight;     // this is no required because are diff name
        this.color = "white";   //  not necessary

    }

    public static void main(String[] args) {
        Duck b = new Duck();
        b.setData(1,2);
        System.out.println(b.length + " " + b.height + " " + b.color);
    }

}
