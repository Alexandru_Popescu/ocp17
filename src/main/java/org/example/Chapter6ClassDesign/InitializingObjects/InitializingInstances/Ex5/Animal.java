package org.example.Chapter6ClassDesign.InitializingObjects.InitializingInstances.Ex5;

public class Animal {

    String name = "Alex";

    {
        System.out.println(name);
    }


    public Animal() {  // fourth
        this(4);
        System.out.println("Fist Constructor from parent");
    }

    {
        System.out.println("Second non-static from parent");
    }

    public Animal(int a) {  //fifth
        this("A");
        System.out.println("Second constructor from parent");
    }

    public Animal(String name) {   //sixth
        System.out.println("Third constructor from parent");
    }

    {
        name = "Ion";
        System.out.println(name);
    }

    static {
        System.out.println("First static from parent");
    }

    {
        System.out.println("Third non-static from parent");
    }


}
