package org.example.Chapter6ClassDesign.InitializingObjects.InitializingInstances.Ex5;


import org.example.Chapter6ClassDesign.InitializingObjects.InitializingInstances.Ex5.Animal;

public class Cat extends Animal {
    int age = 5;

    {
        System.out.println(age);
    }

    static {
        System.out.println("First static context from child");
    }

    {
        System.out.println("second non-static from child");
    }

    static {
        System.out.println("ad");
    }

    public Cat() {    //first
        this(2);
        System.out.println("first constructor from child");
    }

    public Cat(int a) {   //second
        this("A");
        System.out.println("Second constructor from child");
    }

    public Cat(String name) {  //third
        System.out.println("Third constructor from child");
    }


}
