package org.example.Chapter6ClassDesign.InitializingObjects.InitializingInstances.Ex1;

public class Ape extends Primate{

    public Ape(int fur){
        System.out.print("Ape1-");
    }
    public Ape(){
        System.out.print("Ape2-");
    }

}
