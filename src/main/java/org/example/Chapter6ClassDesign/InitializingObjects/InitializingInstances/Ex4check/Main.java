package org.example.Chapter6ClassDesign.InitializingObjects.InitializingInstances.Ex4check;

public class Main {
    public static void main(String[] args) {
        System.out.println("Start the party");
        new Child(3);
        System.out.println("Still counting");
        new Child(4);
    }
}
