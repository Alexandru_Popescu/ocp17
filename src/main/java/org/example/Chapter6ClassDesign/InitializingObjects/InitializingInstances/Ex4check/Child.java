package org.example.Chapter6ClassDesign.InitializingObjects.InitializingInstances.Ex4check;

public class Child extends Parent {
 //1
    static {
        System.out.println("Child");
    }
  //3
    {
        System.out.println("FirstNonStatic");
    }

    public Child(int a){
        super(1);
        System.out.println("Final");
    }

    public Child(){
        System.out.println("SecondConstructorChild");
    }
//2
    static {
        System.out.println("SecondChild");
    }

   //4
    {
        System.out.println("SecondNonStatic");
    }

}
