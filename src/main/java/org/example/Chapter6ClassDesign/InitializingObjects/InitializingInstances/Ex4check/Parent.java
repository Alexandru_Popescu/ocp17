package org.example.Chapter6ClassDesign.InitializingObjects.InitializingInstances.Ex4check;

public class Parent {
//1
    static {
        System.out.println("Parent");
    }
//3
    {
        System.out.println("First");
    }

    public Parent(int a){
        this();
        System.out.println("First Constructor");

    }

    public Parent(){
        this("Alex");
        System.out.println("SecondConstructor");
    }
    public Parent(String name){
        System.out.println("ThirdConstructor");
    }
    //2
    static {
        System.out.println("SecondParent)");
    }

   //4
    {
        System.out.println("Second");
    }

}
