package org.example.Chapter6ClassDesign.InitializingObjects.InitializingInstances.Ex5;

import org.example.Chapter6ClassDesign.InitializingObjects.InitializingInstances.Ex5.Cat;

public class Main {
    public static void main(String[] args) {
        System.out.println("Start the party");
        Cat cat = new Cat();
    }
}
