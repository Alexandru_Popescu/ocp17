package org.example.Chapter6ClassDesign.CreatingAbstracClasses.CreatingConcreteClass.Ex2;

public class Lion extends BigCat{
       String getName() {
        return "Lion";
    }
    protected void roar() {
        System.out.println("The Lion lets out a loud ROAR!");
    }
}
