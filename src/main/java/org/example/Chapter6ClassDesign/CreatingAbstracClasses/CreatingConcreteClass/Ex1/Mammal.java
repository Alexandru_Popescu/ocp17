package org.example.Chapter6ClassDesign.CreatingAbstracClasses.CreatingConcreteClass.Ex1;

public abstract class Mammal {

    abstract void showHorn();
    abstract void eatLeaf();

}
