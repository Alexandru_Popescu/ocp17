package org.example.Chapter6ClassDesign.CreatingAbstracClasses.IntroducingAbstractClasses;

public class Coyote extends Canine{
    public String getSound() {
        return "Roar";
    }
}
