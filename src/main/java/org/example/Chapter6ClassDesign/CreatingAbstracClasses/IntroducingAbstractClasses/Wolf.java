package org.example.Chapter6ClassDesign.CreatingAbstracClasses.IntroducingAbstractClasses;

public class Wolf extends Canine{
    public String getSound() {
        return "Woooooof";
    }
}
