package org.example.Chapter6ClassDesign.CreatingAbstracClasses.IntroducingAbstractClasses;


public class Fox extends Canine{
    public String getSound() {
        return "Squeak";
    }

    public static void main(String[] args) {
        Canine canine = new Fox();
        canine.bark();

    }
}
