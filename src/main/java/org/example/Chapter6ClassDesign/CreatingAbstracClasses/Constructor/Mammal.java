package org.example.Chapter6ClassDesign.CreatingAbstracClasses.Constructor;

public abstract class Mammal {
    abstract CharSequence chew();
    public Mammal(){
        System.out.println(chew());
    }

}
