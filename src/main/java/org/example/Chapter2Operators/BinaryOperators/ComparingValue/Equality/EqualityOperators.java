package org.example.Capitolul2Operators.BinaryOperators.ComparingValue.Equality;

import java.io.File;

public class EqualityOperators {
    public static void main(String[] args) {

        // no mix type
//        boolean monkey = true ==3;
//        boolean ape = false != "Giraffe ";

        boolean bear = false;
        boolean polar = (bear =true);
        System.out.println(polar);
        System.out.println(bear);

        //can we apply to reference not objects

        var monday = new File("schedule.txt");
        var tuesday = new File("schedule.txt");
        var wednesday= tuesday;
        System.out.println(monday == tuesday); //false
        System.out.println(tuesday == wednesday);  //true


        System.out.println(null == null);

   boolean m = true ==false;
        System.out.println(m);


    }

}
