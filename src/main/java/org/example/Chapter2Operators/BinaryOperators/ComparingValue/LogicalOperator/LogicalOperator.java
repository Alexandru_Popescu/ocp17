package org.example.Capitolul2Operators.BinaryOperators.ComparingValue.LogicalOperator;

// & | ^


public class LogicalOperator {
    public static void main(String[] args) {

        boolean a = true;
        boolean b = true;
        boolean c = false;

        boolean res1 = a & b;
        System.out.println(res1); // true
        boolean res2 = a | c;
        System.out.println(res2); // true
        boolean res3 = a ^c;
        System.out.println(res3); // true
    }
}
