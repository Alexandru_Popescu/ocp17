package org.example.Capitolul2Operators.BinaryOperators.ComparingValue.RelationalOperators;


// > < <= >=  --> only for numeric values
public class RelationalOperators {
    public static void main(String[] args) {

        int a =2, b=4,c=2;
        System.out.println(a<b); // true
        System.out.println(a<=b); // true
        System.out.println(a >= c);  //true
        System.out.println(a > c ); // false

    }


}
