package org.example.Capitolul2Operators.BinaryOperators.ComparingValue.RelationalOperators;
// always false


public class NullInstanceOf {
    public static void main(String[] args) {
        System.out.print(null instanceof  Object); // false
        Object noObject = null;
        System.out.print(noObject instanceof String ); // false
//        System.out.print(null instanceof  null); does no compile


    }
}
