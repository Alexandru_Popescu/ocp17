package org.example.Capitolul2Operators.BinaryOperators.ComparingValue;

public class ConditionalOperator {
    // diff between short circuit operator and Logical Operator
    public static void main(String[] args) {
        boolean a = true;
        boolean b = false;
        System.out.println("example 1");
        boolean result = a && m(1);   //1
        boolean result1 = b && m(2);
        System.out.println(result);   // true // 1
        System.out.println(result1);   //false
        System.out.println("---------------");
        System.out.println("Example2");
        boolean result3 = a & m(3);
        boolean result4 = b & m(4);
        System.out.println(result3);  //true si 3
        System.out.println(result4);  // false si 4


    }
    public static Boolean m(int m) {
        System.out.println(m);
        return true;
    }

}

