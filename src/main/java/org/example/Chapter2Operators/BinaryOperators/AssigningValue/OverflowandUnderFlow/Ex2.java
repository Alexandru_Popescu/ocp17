package org.example.Capitolul2Operators.BinaryOperators.AssigningValue.OverflowandUnderFlow;

public class Ex2 {
    public static void main(String[] args) {

        int test = Integer.MAX_VALUE;
        System.out.println(test);
        System.out.println(test + 1);   // -> overflow
        System.out.println(test + 2);   // -> overflow
        System.out.println(test + 3);   // -> overflow
        float check = Float.MIN_VALUE;
        System.out.println(check);
        System.out.println(check - 1);
        float check2 = Float.MAX_VALUE;
        System.out.println(check2 );
        System.out.println(check2+5465);
    }
}
