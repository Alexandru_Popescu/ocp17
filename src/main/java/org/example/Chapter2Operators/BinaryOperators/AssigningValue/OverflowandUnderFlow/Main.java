package org.example.Capitolul2Operators.BinaryOperators.AssigningValue.OverflowandUnderFlow;

public class Main {
    public static void main(String[] args) {
// overflow
        int a = Integer.MAX_VALUE;
        int b = Integer.MIN_VALUE;
        System.out.println(a);
        System.out.println(b);
        System.out.println(a +2 );

        // underflow

        float c = Float.MIN_VALUE;
        float d = c /10;
        System.out.println(d);

        int cookies = 4;
        double reward = 3 +2 * --cookies;
        System.out.print("Zoo animal receives: "+reward+" reward points");

    }
}
