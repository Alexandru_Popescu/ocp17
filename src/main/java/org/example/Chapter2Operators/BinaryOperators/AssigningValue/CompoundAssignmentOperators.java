package org.example.Capitolul2Operators.BinaryOperators.AssigningValue;

/*
  += -= *= /=

  --> variable should be declared first, after that we can use with Compound
 */
public class CompoundAssignmentOperators {
    public static void main(String[] args) {

        int camel = 2, giraffe = 3;
//        camel = camel * giraffe;
//        System.out.println(camel);
        camel *= giraffe;
        System.out.println(camel);


// save have explicitly cast a value

        long goat = 10;
        int sheep = 5;
        sheep = (int) (sheep * goat);
        System.out.println(sheep);

        long goat2 = 10;
        int sheep2 = 5;
        sheep2 *= goat2;
        System.out.println(sheep2);
    }
}
