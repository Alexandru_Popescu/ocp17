package org.example.Capitolul2Operators.BinaryOperators.AssigningValue;

public class ExercisesNumericPromotionAndCasting {
    public static void main(String[] args) {
        //Ex1   --> data type small to large data type
        int a = 1;
        byte b = 2;
        int result = a + b;

        //Ex2  --> cast

        // float egg = 2.0/9;  --> punem double in float
        //int tadpole = (int)5 *2l ;  long in int
        // short frog = 3-2.0 //
        long c = (long) 12123213123213l;


    }
}
