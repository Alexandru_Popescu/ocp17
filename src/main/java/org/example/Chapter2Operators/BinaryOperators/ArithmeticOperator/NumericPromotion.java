package org.example.Capitolul2Operators.BinaryOperators.ArithmeticOperator;

public class NumericPromotion {
    public static void main(String[] args) {
        //ex 1 -> The result will be int
        short a = 3;
        int b = 34;
        int result = a + b;
        //ex 2 -> if we have integral number and floating-point always will be floating-point

        int x = 4;
        double d = 3.4;
        var result2 = d + x;
        System.out.println(result2);

        //ex 3 byte, short and char -> always will be int
    }


    void check() {
        short w = 14;
        float x = 13;
        double y = 30;

        var z = w * x / y;
        int a = (int) z;


    }


    void n() {
        //ex 1
//
//        int a = 3;
//        long b =6l;
//        var c = a + b;
//        int reult = c;  --> primeaza tipul de variabila mai mare

        //ex 2
//
//        int a = 4;
//        double b = 3;
//        var c = a + b;
//        int result = c;   --> primeaza floating point instead integral value

        //ex 3

//        short a = 4;
//        byte b =3;
//        var c = a + b;
//        short result = c;  --> byte, short and char automatomat vor fi promote int


    }


}
