package org.example.Capitolul2Operators.BinaryOperators.ArithmeticOperator;

public class ArithmeticOperators {

    /*
     +      -      *      /      %        -->  *, / % > +,-
     */

    //  nu se aplica boolean expression and + += String
    public static void main(String[] args) {

        int a = 2 *5 + 3* 4 -8;
        System.out.println(a);

        // adding parentheses have prior
        int b = 2 * ((5 + 3)* 4 -8);
        System.out.println(b);

        // int c =  2 * [(3 *2) +5];
    }
}
