package org.example.Capitolul2Operators.TernaryOperator;

public class Ex2 {
    public static void main(String[] args) {

        int owl = 5;
        int food = owl < 6 ? owl > 9 ? 3 : 4 : 5;
        System.out.println(food);


        // the second and third data type should pe same

        int stripes = 7;
        System.out.print((stripes > 7) ? 21 :"Zebra");   // -> convert both data types in Object values
//         int animal = (stripes <9) ? 3 : "horse";  // no compile

    }
}
