package org.example.Capitolul2Operators.TernaryOperator;

public class UnperformedSideEffects {
    public static void main(String[] args) {

        int sheep = 1;
        int zzz = 1;
//        int sleep = zzz <10 ? sheep++ : zzz++;
//        System.out.print(sheep + ","  + zzz);
        System.out.println("----");
        int sleep2 = zzz >10 ? sheep++ : zzz++;
        System.out.println(sheep + ","  + zzz);

    }
}
