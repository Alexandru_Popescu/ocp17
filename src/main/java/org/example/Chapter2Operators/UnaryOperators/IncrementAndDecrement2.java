package org.example.Capitolul2Operators.UnaryOperators;
// high order of procedure
public class IncrementAndDecrement2 {
    public static void main(String[] args) {

  int a = 3;
  int b=5;
  int c=2;

  int result = ++a + ++a + c++ + ++c + ++b + ++b +c +b+ a++;

  //      4 + 5 + 2 +4 + 6+7 +4+7+5
         // a =5
        //b = 7
        //c = 4
        System.out.println("result = " +result);

        System.out.println("a = " + a);
        System.out.println("b = " + b);
        System.out.println("c = " + c);
    }
}
