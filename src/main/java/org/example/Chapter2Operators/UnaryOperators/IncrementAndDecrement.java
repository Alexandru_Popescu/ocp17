package org.example.Capitolul2Operators.UnaryOperators;
// high order of procedure
public class IncrementAndDecrement {
    public static void main(String[] args) {

        int a = 0;
        System.out.println(a);    // 0
        System.out.println(++a);   // 1
        System.out.println(a);    //1
        System.out.println(a++);  //1
        System.out.println(--a);  //1
        System.out.println(a);   //1
        System.out.println(a--);   //1


    }
}
