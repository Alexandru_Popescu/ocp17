package org.example.Capitolul2Operators.UnaryOperators;

public class Casting {
    public static void main(String[] args) {

        int fur = (int) 5;
        int hair = (short) 2;
        String type = (String) "Bird";
        short tail = (short) (4 + 10);

        /*
         *  long feathers = 10 (long);  --> always in left side
         *  float egg = 2.0 / 9;   --> in right side is double --> we need cast
         *  int tadpole = (int)5 * 2l;
         * short frog = 3 - 2.0;
         * int fish  = 9f;
         * long reptile = (long)123456789101112;
         */


    }
}
