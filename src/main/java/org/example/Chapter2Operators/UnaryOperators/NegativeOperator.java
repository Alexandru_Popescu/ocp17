package org.example.Capitolul2Operators.UnaryOperators;

public class NegativeOperator {
    public static void main(String[] args) {

        double zooTemperature = 1.21;
        System.out.println(zooTemperature);
        zooTemperature = -zooTemperature;
        System.out.println(zooTemperature);
        zooTemperature = -(-zooTemperature);
        System.out.println(zooTemperature);
  // nu se poate aplica - operator to boolean expression or logical complement operator to numeric expression

//  int a = !5;  --> just boolean
// boolean a = -true;



    }
}
