package org.example.Chapter5Methods.WorkingWithVarags;



public class CallingMethodsWithVarargs {

    public static void main(String[] args) {
        int [] numbers = {1,2,3};
        var callingMethodsWithVarargs = new CallingMethodsWithVarargs();
        callingMethodsWithVarargs.walk1(numbers);
        callingMethodsWithVarargs.walk1(1,3,4);
        callingMethodsWithVarargs.walk1();
    }



    public void walk1(int ...a){
        for (int print : a){
            System.out.print(print);
        }

        System.out.println("---");
        System.out.println(a.length);
    }

}
