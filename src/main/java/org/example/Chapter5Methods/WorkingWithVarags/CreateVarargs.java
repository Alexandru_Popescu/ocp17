package org.example.Chapter5Methods.WorkingWithVarags;

/*
two rules: only one parameter should be varargs and must be the last parameter
 */

public class CreateVarargs {
    public static void main(String[] args) {

    }

    public void walk1(int ...a){}
    public void walk2(int b,int ...a){}
//    public void walk3(int ...a,int b){}
//    public void walk3(int ...a,int... b){}
}
