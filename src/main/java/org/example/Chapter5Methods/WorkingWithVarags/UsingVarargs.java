package org.example.Chapter5Methods.WorkingWithVarags;

public class UsingVarargs {
    public static void main(String[] args) {
        varargs(1);
        varargs(1,2);
        varargs(1,1,2);
        varargs(1,new int [] {1,3});
//        varargs(1,null);
    }

    static void varargs(int start, int...steps){
        System.out.print(steps.length);

    }
}
