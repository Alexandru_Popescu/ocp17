package org.example.Chapter5Methods.WorkingWithVarags;

public class AccessingElementsOfVarargs {

    public static void main(String[] args) {
        var accessingElementsOfVarargs =new AccessingElementsOfVarargs();
        accessingElementsOfVarargs.m(1,2,3);
    }

    void m (int ... a){
        System.out.println(a[1]);
    }

}
