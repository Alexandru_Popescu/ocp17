package org.example.Chapter5Methods.ApplyingAccessModifiers.Private;

public class Private {

    private String noise ="quack";
    private void quack(){
        System.out.println(noise);
    }

}
 class BadDuck{

     public static void main(String[] args) {
         BadDuck badDuck = new BadDuck();

     }
    void n(){
         BadDuck badDuck = new BadDuck();
         Private p = new Private();

//         badDuck.quack    --> private restrict per file

    }


 }