package org.example.Chapter5Methods.ApplyingAccessModifiers.Protected.B;

import org.example.Chapter5Methods.ApplyingAccessModifiers.Protected.A.A;

public class B extends A {

    public void check() {
        m();
        System.out.println(name);
    }

    public static void main(String[] args) {
        B b = new B();
        b.m();
        System.out.println(b.name);
        b.check();

        A a = new B();
        // a.m --> the type of variable should be subclass
    }


}
