package org.example.Chapter5Methods.ApplyingAccessModifiers.Protected.A;

public class C {
    public static void main(String[] args) {
        A a = new A();
        System.out.println(a.name);
        a.m();
    }
}
