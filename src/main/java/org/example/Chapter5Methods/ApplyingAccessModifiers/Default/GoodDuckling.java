package org.example.Chapter5Methods.ApplyingAccessModifiers.Default;

public class GoodDuckling{
    void makeNoise(){
        var goodDuck  = new MotherDuck();
        System.out.println(goodDuck.noise);
        goodDuck.quack();
        var motherDuck = new MotherDuck();
        System.out.println(motherDuck.noise);
        motherDuck.quack();
        GoodDuckling goodDuckling = new GoodDuckling();
        goodDuckling.makeNoise();

    }

}
