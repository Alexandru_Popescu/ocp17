package org.example.Chapter5Methods.OverloadingMethods.Declare;
/*
for overloading needs only name to be the same and parameter diff.--> Method signature it's important
 */




public class Falcon {
    public void fly(int numMiles) {
    }

    public void fly(short numMiles) {
    }

    public boolean fly() {
        return true;
    }

    void fly(int numMiles, short numFeet) {
    }

    public void fly(short numFeet, int numMiles) throws Exception {
    }
}
