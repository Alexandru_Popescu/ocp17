package org.example.Chapter5Methods.OverloadingMethods.RefferenceType;

import java.time.LocalDate;
import java.time.Month;
import java.util.*;

public class Parrot {
    public static void print(List<Integer> i) {
        System.out.print("I");
    }

    public static void print(CharSequence c) {
        System.out.print("C");
    }

    public static void print(Object o) {
        System.out.print("O");
    }

    public static void main(String[] args) {
        Parrot parrot = new Parrot();
        parrot.print("ABC");
        parrot.print(Arrays.asList(1, 2, 3));
        parrot.print(LocalDate.of(2022, Month.NOVEMBER, 22));

    }

}
