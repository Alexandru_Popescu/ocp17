package org.example.Chapter5Methods.OverloadingMethods.RefferenceType;

 public class Pelican {

    public void fly(String name){
        System.out.print("String");
    }

    public void fly(Object name){
        System.out.println("Object");
    }



    public static void main(String[] args) {
        Pelican pelican  = new Pelican();
        pelican.fly("test");
        System.out.print("-");
        pelican.fly(46); // first search for int, after Integer, and after for Object
    }

}
