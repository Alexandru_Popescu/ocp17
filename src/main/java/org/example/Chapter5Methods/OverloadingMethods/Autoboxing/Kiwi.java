package org.example.Chapter5Methods.OverloadingMethods.Autoboxing;

public class Kiwi {
    public void fly(int numMiles){
        System.out.println("int");
    }
    public void fly(Integer numMiles){
        System.out.println("Integer");
    }

    public static void main(String[] args) {
        Kiwi kiwi = new Kiwi();
        kiwi.fly(2);   // Java do not extra work to autoboxing is a method with int parameter exists
    }
}
