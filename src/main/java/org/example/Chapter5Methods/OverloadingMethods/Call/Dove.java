package org.example.Chapter5Methods.OverloadingMethods.Call;

//Calling overloading method is easy: Java after you add the code, calls the right one:


public class Dove {

    public void fly(int num){
        System.out.println("it's int");
    }

    public void fly(short num){
        System.out.println("it's short");
    }



    public static void main(String[] args) {
   Dove dove  = new Dove();
   dove.fly((short) 1);
    }


}
