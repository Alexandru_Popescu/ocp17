package org.example.Chapter5Methods.DesigningMethods;

public class Overview {
    public static void main(String[] args) throws InterruptedException {

        Overview overview = new Overview();
        overview.nap(20);
    }

    public final void nap (int minutes) throws InterruptedException{

        //take nap
        System.out.println(minutes);
    }

}
