package org.example.Chapter5Methods.DesigningMethods;

public class ReturnType {
    public static void main(String[] args) {
        System.out.println(ReturnType.hike(50));
    }

    void m() {
        return;
    }

    static String hike(int a) {
        if (1<20) return "Orange";
        return "apple";
    }

     int getHeight(){
//        int temp = 9L;
//       long temp = 9 ;
//        return temp;
        return 10;
    }

}
