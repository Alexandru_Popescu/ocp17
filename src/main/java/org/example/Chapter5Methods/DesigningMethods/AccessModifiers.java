package org.example.Chapter5Methods.DesigningMethods;
/*
* private --> method called only in the same class
* package access --> only in the same package (default package)
* protected --> in the same package or a subclass
* public --> anywhere
 */


public class AccessModifiers {

    public void skip(){}
//    default void skip2(){} --> never used as an access modifiers, only in switch and interfaces
//    void public skip3(){} --> after return type
    void skip4(){}
}
