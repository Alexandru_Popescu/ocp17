package org.example.Chapter5Methods.PassingDataAmongMethods.PassByValue;

/*
pass-by-value --> a copy is made and send to the method received.
If in code you change the assignment, does not affect the caller
 */

public class Main {

    public static void main(String[] args) {
        int num= 4;
        newNumber(num);
        System.out.println(num);

    }
    public static void newNumber(int num){
        num = 8;

    }
}
