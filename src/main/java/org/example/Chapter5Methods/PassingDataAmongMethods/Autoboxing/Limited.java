package org.example.Chapter5Methods.PassingDataAmongMethods.Autoboxing;

public class Limited {
    public static void main(String[] args) {

        Integer ear = Integer.valueOf(9);   // first is unboxing
        long e = ear;                           // second numeric promotion

        // Long a = 8;    // java cannot do unboxing and cast in the same time.

    }
}
