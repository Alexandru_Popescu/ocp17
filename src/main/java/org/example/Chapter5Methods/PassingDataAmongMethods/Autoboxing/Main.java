package org.example.Chapter5Methods.PassingDataAmongMethods.Autoboxing;

// autoboxing = convert a primitive to its equivalent wrapper class
// unboxing == convert a wrapper class to its equivalent primitive


public class Main {

//    int quack = 5;
//    Integer quackQuack = Integer.valueOf(quack);   // Convert int to Integer
//    int quackQuackQuack= quackQuack.intValue();    // convert Integer to Int

    int quack = 5;
    Integer quackQuack = quack;  // autoboxing
    int quackQuackQuack = quackQuack; // unboxing

    Short tail = 8;     // autoboxing
    Character p = Character.valueOf('p');
    char paw = p;       // unboxing

    Boolean nose = true;  //autoboxing
    Integer e = Integer.valueOf(9);
    long ears = e;     // unboxing and implicit casting


}
