package org.example.Chapter5Methods.PassingDataAmongMethods.Autoboxing;

public class Methods {

    public static void m (Long l){
        System.out.println(l);
    }

    public static void n (Integer l){
        System.out.println(l);
    }
    public static void o (int l){
        System.out.println(l);
    }

    public static void main(String[] args) {
        Methods methods = new Methods();
        methods.m(123l);
        methods.n(123);
        methods.o(123);
    }

}
