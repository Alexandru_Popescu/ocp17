package org.example.Chapter5Methods.PassingDataAmongMethods.PassObject.Method;


public class Dog {

    public static void main(String[] args) {
        var name = new StringBuilder("Webby ");
        speak(name);
        System.out.println(name);

    }

    public static void speak(StringBuilder s){
        s.append("Word");
    }

}
