package org.example.Chapter5Methods.PassingDataAmongMethods.PassObject;

public class Main {

    public static void main(String[] args) {
        String name = "Dog";
        speak(name);
        System.out.println(name);

    }

    public static void speak(String name){
        name="Tom";
    }

}
