package org.example.Chapter5Methods.AccessingStaticData.ClassVsInstance;
/*
rules: Non-static member can access static member because is not required an object to use it.
     Static member cannot access non-static member because needs an instance. With reference variable can access.

 */

public class InstanceMember {
public static String name;

    public static void m() {
        System.out.println("Hello");
    }

    public void n() {
        m();
        check();
        name="alex";
    }

    public static void main(String[] args) {
        InstanceMember instanceMember = new InstanceMember();
        instanceMember.n();
    }

    void check() {
    }


}
