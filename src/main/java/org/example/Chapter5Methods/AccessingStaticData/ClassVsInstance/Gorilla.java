package org.example.Chapter5Methods.AccessingStaticData.ClassVsInstance;

public class Gorilla {

    public static int count;
    public static void addGorilla(){
        count++;
    }
    public void babyGorilla(){
        count++;
    }
    public void announceBabies(){
        addGorilla();
        babyGorilla();
    }
    public static void announceBabiesToEveryone(){
        addGorilla();
        Gorilla gorilla = new Gorilla();
        gorilla.babyGorilla(); //because is non-static only with reference variable can be access it.

    }

    public static int total;   // I added to solve the below error static for total variable
    public static double average = total /count;


}
