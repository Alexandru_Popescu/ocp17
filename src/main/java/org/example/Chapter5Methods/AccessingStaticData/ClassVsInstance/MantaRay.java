package org.example.Chapter5Methods.AccessingStaticData.ClassVsInstance;

public class MantaRay {
    private String name ="Sammy";
    public static void first(){}
    public static void second(){}
    public void third (){
        System.out.println(name);
    }

    public static void main(String[] args) {
        first();
        second();
        var mantaRay = new MantaRay();
        mantaRay.third();


        /*
         // in you are in static member, you cannot access directly instance members.
         All make them static as well, ot through reference variable .
         */


    }

}
