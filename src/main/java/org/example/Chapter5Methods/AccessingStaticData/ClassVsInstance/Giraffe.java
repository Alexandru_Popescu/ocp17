package org.example.Chapter5Methods.AccessingStaticData.ClassVsInstance;

public class Giraffe {

    public static void main(String[] args) {
        Giraffe giraffe = new Giraffe();
        System.out.println(giraffe);
    }
    public void eat(Giraffe g) {
        g.drink();
        allGiraffeGoHome(new Giraffe());
        allGiraffeComeOut();

    }
    public void drink() {

        eat(new Giraffe());
        allGiraffeComeOut();
        allGiraffeComeOut();
    }
    public static void allGiraffeGoHome(Giraffe g) {
        allGiraffeComeOut();
        g.eat(new Giraffe());
        g.drink();

    }
    public static void allGiraffeComeOut() {
        allGiraffeGoHome(new Giraffe());
        Giraffe giraffe = new Giraffe();
        giraffe.drink();
        giraffe.eat(new Giraffe());
    }

}
