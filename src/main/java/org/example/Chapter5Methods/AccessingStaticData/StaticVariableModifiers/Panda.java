package org.example.Chapter5Methods.AccessingStaticData.StaticVariableModifiers;

/*
  final static means --> constant -> never change during the program execution
 */

public class Panda {

    private static final int NUM_BUCKETS =35;

    public static void main(String[] args) {
//        NUM_BUCKETS=34;
    }

}
