package org.example.Chapter5Methods.AccessingStaticData.StaticVariableModifiers;

public class ZooInventoryManager {

    private final static String[] treats = new String[10];

    public static void main(String[] args) {
        treats[0]= "A";   // -> we change contents, no reference
    }

}
