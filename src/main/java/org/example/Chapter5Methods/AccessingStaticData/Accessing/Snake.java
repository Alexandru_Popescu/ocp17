package org.example.Chapter5Methods.AccessingStaticData.Accessing;

public class Snake {

    public static long hiss =2;

    public static void main(String[] args) {
        Snake snake = new Snake();
        System.out.println(snake.hiss);
        snake = null;
        System.out.println(snake.hiss);
        System.out.println(Snake.hiss = 3);
    }
}
