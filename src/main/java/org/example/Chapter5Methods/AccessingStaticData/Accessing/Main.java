package org.example.Chapter5Methods.AccessingStaticData.Accessing;

public class Main {

    static int check =1;

    public static void main(String[] args) {
        Main.check =3;
        Main main = new Main();
        main.check = 3;
        Main main1 = new Main();
        main1.check = 4;
        System.out.println(Main.check);
    }
}
