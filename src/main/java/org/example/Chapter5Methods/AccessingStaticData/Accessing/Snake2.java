package org.example.Chapter5Methods.AccessingStaticData.Accessing;

public class Snake2 {

    public static long hiss = 2;

    public static void main(String[] args) {
        Snake2.hiss = 4;

        Snake2 snake1 = new Snake2();
        System.out.println( Snake2.hiss = 6);
        Snake2 snake2 = new Snake2();
        snake2.hiss = 5;
        System.out.println(snake1.hiss);
        System.out.println(snake2.hiss);

    }
}
