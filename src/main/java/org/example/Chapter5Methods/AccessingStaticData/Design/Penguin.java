package org.example.Chapter5Methods.AccessingStaticData.Design;

public class Penguin {

    String name;
    static String taller;


    public static void main(String[] args) {

       var p1= new Penguin();
        p1.name="Lilly";
        System.out.println(p1.taller = "Lilly first");
        var p2 = new Penguin();
        p2.name="Willy";
        p2.taller = "Willy";
        System.out.println(p1.name);
        System.out.println(p1.taller);
        System.out.println(p2.name);
        System.out.println(p2.taller);

    }


}
