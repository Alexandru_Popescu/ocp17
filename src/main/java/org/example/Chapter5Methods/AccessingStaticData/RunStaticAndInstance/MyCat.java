package org.example.Chapter5Methods.AccessingStaticData.RunStaticAndInstance;

public class MyCat {
    public String name;
    public static int age;

    public MyCat(String name) {
        this.name = name;
    }

    {
        System.out.println("My cat is pretty");
    }
    static {
        System.out.println("Run");
    }

    static {
     age = 4;
    }

    {
        System.out.println("Non static ");
    }
    static {
        System.out.println(age = 5);
    }
    @Override
    public String toString() {
        return "MyCat{" +
                "name='" + name + '\'' +
                '}';
    }
}
