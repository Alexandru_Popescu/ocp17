package org.example.Chapter5Methods.AccessingStaticData.StaticImports;
import java.util.List;
//import  java.util.Arrays;
import static java.util.Arrays.asList;

public class ZooParking {

    public static void main(String[] args) {
        List <String> list = asList("one", "two");
//        List <String> list2 = Arrays.asList("one", "two");
    }



    /*
    1 are only for static member as method and variable (Regular imports are for classes)
    import static java.util.Arrays; --> no compile
    2. static import is not correct --> only import static
    3.
       import static java.util.Arrays.asList;
       import java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        Arrays.asList("one");
    }
}
    4. we cannot import a method with the same name for 2 classes


     */
}
