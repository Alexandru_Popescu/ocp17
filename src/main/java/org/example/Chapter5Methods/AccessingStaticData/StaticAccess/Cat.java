package org.example.Chapter5Methods.AccessingStaticData.StaticAccess;

public class Cat {

    private static String name = "Alex";

    static {
        System.out.println("block 2");
    }

    private static void staticContext() {
        System.out.println("Static Context");
    }

    static {
        System.out.println("block 1 ");
    }

    private static int age = age();

    private static int age() {
        return 10;
    }

    public static void main(String[] args) {
        System.out.println(age);
        System.out.println(name);
        staticContext();
    }

}
