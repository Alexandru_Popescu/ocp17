package org.example.Chapter5Methods.AccessingStaticData.StaticInitializers;

public class Static {
    private static int one;
    private static final int two;
    private static final int three = 3;
//    private static final int four;  --> compile error because isn't assigned

    static {
        one = 1;
        two = 2;
//        three=4;   //  we cannot change the value
//        two=34;   // is already assigned on line 11
        one = 2;

    }

}
