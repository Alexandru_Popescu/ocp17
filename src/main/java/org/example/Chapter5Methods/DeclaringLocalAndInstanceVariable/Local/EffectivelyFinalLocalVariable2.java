package org.example.Chapter5Methods.DeclaringLocalAndInstanceVariable.Local;

public class EffectivelyFinalLocalVariable2 {
    public static void main(String[] args) {
        var effectivelyFinalLocalVariable2 = new EffectivelyFinalLocalVariable2();
        effectivelyFinalLocalVariable2.zooFriends();

    }

    public void zooFriends() {
        final int size;
        if (1 > 2) size = 5;
        else size = 8;
        System.out.println(size);

    }
}
