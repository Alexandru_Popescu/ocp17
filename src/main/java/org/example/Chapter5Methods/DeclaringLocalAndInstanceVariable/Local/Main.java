package org.example.Chapter5Methods.DeclaringLocalAndInstanceVariable.Local;

public class Main {
    public static void main(String[] args) {

    }

    public void zooAnimalsCheckup(boolean isWeekend){
        final int rest;
        if (isWeekend) rest =5; else rest=20;
        System.out.println(rest);

         final var giraffe = new Main();
         final  int friends [] = new int[3];

//         rest =10;
//         giraffe = new Main();
//         friends = null;

//        if (isWeekend) rest =10;       //needs an assigned if is false
//        System.out.println(rest);


    }

}
