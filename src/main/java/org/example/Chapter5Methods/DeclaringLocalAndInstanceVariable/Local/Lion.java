package org.example.Chapter5Methods.DeclaringLocalAndInstanceVariable.Local;

public class Lion {
    int hunger = 4;

    public static void main(String[] args) {
        Lion lion = new Lion();
        System.out.println(lion.feedZooAnimals());
        System.out.println(lion.hunger);
    }


    public int feedZooAnimals() {

        int snack = 10;
        if (snack>4) {
            long dinnerTime = snack++;
            hunger--;
        }
        return snack;
    }

}
