package org.example.Chapter5Methods.DeclaringLocalAndInstanceVariable.Local;

public class EffectivelyFinalLocalVariable {
    public static void main(String[] args) {


    }

    public String zooFriends() {
        String name = "Harry the Hippo";    //works add final
        int size = 5;    //not works
         boolean wet;    // works
        if (size > 100) size++;
        name.substring(0);
        wet = true;
        return name;


    }
}
