package org.example.Chapter5Methods.DeclaringLocalAndInstanceVariable.Instance;

/*
 *can have access modifiers
 * Optional specifiers are : final, volatile, transient
 * we can initialize directly or when the object is initialized.
 * when we use final, they don't have default implementation


 */


public class PolarBear {

    final int age = 10;
    final int fishEaten;
    final String name;

    {
        fishEaten = 10;
    }

    public PolarBear() {
        name = "Robert";
    }
}
