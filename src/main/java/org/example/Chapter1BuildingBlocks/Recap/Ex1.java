package org.example.Chapter1BuildingBlocks.Recap;

public class Ex1 {
    {
        System.out.println("A");
    }
    int x = 4;
    {
        System.out.println(x = 5);
    }
    public Ex1() {
        x = 6;
        System.out.println("Check");
    }
    public static void main(String[] args) {
        Ex1 ex1 = new Ex1();
        System.out.println("some");
        System.out.println(ex1.x);
    }


    /* output
    A
     5
     check
     some
     6
     */

}
