package org.example.Chapter1BuildingBlocks.UnderstandDatatypes;

public class GlobalVariableDefaultValue {

    String name;
    float a;
    double b;
    char c;
    long d;
    float e =0.0f;

    public static void main(String[] args) {
        GlobalVariableDefaultValue globalVariableDefaultValue = new GlobalVariableDefaultValue();
        System.out.println("Float value = " + globalVariableDefaultValue.a);
        System.out.println(globalVariableDefaultValue.name);
        System.out.println(globalVariableDefaultValue.b);
        System.out.println(globalVariableDefaultValue.c);
        System.out.println(globalVariableDefaultValue.d);
        System.out.println("Float value = " +globalVariableDefaultValue.e);
    }

}
