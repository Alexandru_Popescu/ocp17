package org.example.Chapter1BuildingBlocks.UnderstandDatatypes;

public class ChangeTheBase {
    public static void main(String[] args) {

        //hexagonal  prefix 0X and digits 0-9 and A-F/a-f  --> is insensitive case
        int a = 0x34;
        int b = 0x3f;
        int z = 0x3F;
        System.out.println(z);
        int c = 0x89F;
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);

        //binary
        int d = 0b01;
        int e = 0B01;
        System.out.println(d);
        System.out.println(e);
    }
}
