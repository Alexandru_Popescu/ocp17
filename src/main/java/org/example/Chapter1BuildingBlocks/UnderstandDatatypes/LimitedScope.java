package org.example.Chapter1BuildingBlocks.UnderstandDatatypes;

public class LimitedScope {
    public static void main(String[] args) {


    }

    public void eatIfHungry(boolean hungry) {
        if (hungry) {
            int biteOfCheese = 1;
            {
                var teenyBit = true;
                System.out.println(biteOfCheese);


            }

        }
//        System.out.println(teenyBit);

    }
}