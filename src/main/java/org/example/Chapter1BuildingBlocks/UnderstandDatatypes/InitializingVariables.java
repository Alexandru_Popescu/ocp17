package org.example.Chapter1BuildingBlocks.UnderstandDatatypes;

public class InitializingVariables {
    public static void main(String[] args) {

//        final int [] a = new int[5];     --> if is final we cannot change the SIZE
//        a[0] = 1;
//        a= null;
//    }


        //  --> below is an example how a local variable can be unreachable. Compile is smart to see if there is a situation where b could be false
    int a;
    int b;
    int c;
    int result=5;
   if (result >3){
       a=5;
       b=3;
   }else {
       a=6;
   }
        System.out.println(a);
//        System.out.println(b);
    }
}