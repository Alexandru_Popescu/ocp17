package org.example.Chapter1BuildingBlocks.UnderstandDatatypes;

public class ManagingVariableScope {
    public static  void main(String[] args) {


    /*

     Local variable -> nu pot fi utilizate in exteriorul metodei
    Integer m(int n){
        int x;
        return 1;
    }
    */

        /*
        Exemplu in If. Scopul unei variabile este {}. Fiecare {} are propriul scop


        if (3 > 4) {
            int x =1;
        }{
            System.out.println("Not");
        }
        System.out.println(x);
     */

    }

    void eat(boolean hungry) {
        if (hungry) {
            int bite = 1;
            {
                var a = true;
                System.out.println(bite);
            }
        }
//        System.out.println(a);
    }
}
