package org.example.Chapter1BuildingBlocks.UnderstandDatatypes;

public class OrderOfInitialization2 {

    OrderOfInitialization2(){
        number =5;
    }

    public static void main(String[] args) {
        OrderOfInitialization2 o = new OrderOfInitialization2();
        System.out.println(o.number);
    }

    private int number = 3;
    {number = 4;}

}
