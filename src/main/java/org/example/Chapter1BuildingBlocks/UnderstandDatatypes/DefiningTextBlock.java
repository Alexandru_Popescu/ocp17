package org.example.Chapter1BuildingBlocks.UnderstandDatatypes;

// it used in SQL queries
/*

//-->  /*** --> ***


 */





public class DefiningTextBlock {
    public static void main(String[] args) {
        int i ;
        String text= "Alexandru \nlearn Java";
        System.out.println(text);
        String a = """
                Alexandru
                 learn Java """;

        System.out.println(a);
        String check = "\"Alexandru\"";
        System.out.println(check);

String forMe= "\"Alex\"";
        System.out.println(forMe);

        String blocks = """
           doe\"\"\"
            \"deer\"""
            """;
        System.out.println(blocks);


    }


}
