package org.example.Chapter1BuildingBlocks.UnderstandDatatypes;

public class OrderOfInitialization {

    private String name = "Alex";
    {System.out.println("settings fields");}

    public OrderOfInitialization(){
        name = "Tom";
        System.out.println("setting constructor");
    }

    public static void main(String[] args) {
        var order = new OrderOfInitialization();
        System.out.println(order.name);

    }
}
