package org.example.Chapter1BuildingBlocks.UnderstandDatatypes;

/*
Identifiers
1. begin with letter, currency symbol and "_",
2 not begin with number
3. No keywords
4. No single _
 */


public class DeclaringVariable {
    public static void main(String[] args) {

//        int i; String z;
//      -> not works  int i, String z;   --> needs the same type
//        > not works double d1, double d2;  --> we need to share one type
 int i1;
 int i2;
 int i3;

    }
}
