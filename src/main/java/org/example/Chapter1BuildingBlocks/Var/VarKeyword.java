package org.example.Chapter1BuildingBlocks.Var;
        /*
        rules: only local variable, no allow multiple declaration, we cannot re-assign null (only for Object), only for type is reserved word

         */
public class VarKeyword {

    //    var check="Alex";  -->only local variable
    public static void main(String[] args) {
        // se poate pune si pe 2 linii
      
        var name = "a";
        var a
                = 3;
//    var b =null;   --> nu se poate pune null, fiindca null se pune ptr obiecte, iar compilatorul nu stie ce tip de Obiect are b
        VarKeyword varKeyword = new VarKeyword();
        var b = (VarKeyword) null;
        var text = "";
        text = null;

    }

//  class var{}  --> nu se poate pune var ptr type, nu se pune nici return type
//public var m(){
//        return 1;
//    }


}
