package org.example.Chapter3MakingDecisions.RepetitiveControlStructure.While.whileStatement;

public class Ex2 {
    public static void main(String[] args) {


        int counter = 0;

        while (counter < 10) {
            double price = counter * 10;
            System.out.println(price);
            counter++;
        }

        /*
        0
        10,20,30,40... 90
         */

    }
}
