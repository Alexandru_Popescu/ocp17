package org.example.Chapter3MakingDecisions.RepetitiveControlStructure.While.whileStatement;

public class Ex1 {
    public static void main(String[] args) {

        Ex1 ex1 = new Ex1();
        ex1.eatCheese(5);
    }

    int roomInBelly = 5;

    public void eatCheese(int bitesOfCheese) {
        while (bitesOfCheese > 0 && roomInBelly > 0) {
            bitesOfCheese--;
            roomInBelly--;

        }
        System.out.println(bitesOfCheese + " pieces of cheese left");
    }
}
