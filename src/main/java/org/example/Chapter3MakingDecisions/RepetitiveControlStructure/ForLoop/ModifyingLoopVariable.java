package org.example.Chapter3MakingDecisions.RepetitiveControlStructure.ForLoop;

public class ModifyingLoopVariable {
    public static void main(String[] args) {
//
//        for (int i = 0; i <10; i++){
//            i=0;
//            System.out.println(i);
//
//        }

        for (int i = 0; i <10; i++){
            i++;
            System.out.println(i);

        }

    }
}
