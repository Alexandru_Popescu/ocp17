package org.example.Chapter3MakingDecisions.RepetitiveControlStructure.ForLoop;

public class WorkWithForLoops {
    public static void main(String[] args) {

        /* Example 1
        create infinite loop

        for( ; ;)      --> ; required
        System.out.println("Hello world");

         */


        /* Example 2
       adding multiple Terms to the for Statement


        int x = 0;
        for (long y =0, z =4; x<5 && y <10; x++, y++){
            System.out.println(y + " ");
        }
        System.out.println(x + "x ");

       // output : y = 0, 1,2,3,4
        // x = 5

*/

         /* Example 3
       Rewrite a Variable in the initialization block
         int x = 0;
         for (int x = 4; x <5; x++)
               System.out.println(x);

         */
        int x = 0;
        for (x = 4; x < 5; x++)
            System.out.println(x);

/* Example 4
       Using Incompatible Data Types in the Initialization block
         int x = 0;
         for (Long y = 0, int z = 4; x <5; x++)
               System.out.println(x);

         */

/* Example 5
       Using Loop variable outside the Loop
              for (Long y = 0, x = 4; x <5; x++)
               System.out.println(y);
               System.out.println(x);

         */

    }
}
