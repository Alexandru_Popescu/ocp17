package org.example.Chapter3MakingDecisions.RepetitiveControlStructure.ForEach;

public class Ex1 {
    public static void main(String[] args) {

        int[] a = {1, 2, 3, 4, 5};
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }

        for (int i : a) {
            System.out.print( "\n"  + i + " ");
        }

    }

}
