package org.example.Chapter3MakingDecisions.ControllingFlowWithBranching.Continue;

public class CleaningSchedule3 {
    public static void main(String[] args) {

        CLEANING:
        for (char stables = 'a'; stables <= 'd'; stables++) {
            for (int leopard = 1; leopard < 4; leopard++) {
                System.out.println("Cleaning:  " + stables + ", " + leopard);
            }
        }
    }
}
