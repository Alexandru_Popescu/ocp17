package org.example.Chapter3MakingDecisions.ControllingFlowWithBranching.NestedLoop;

public class ExampleWhile {
    public static void main(String[] args) {

        int hungryHippopotamus = 8;
        while (hungryHippopotamus > 0) {
            do {
                hungryHippopotamus -= 2;
            } while (hungryHippopotamus > 5);
            hungryHippopotamus--;
            System.out.print(hungryHippopotamus + ", ");
        }
    }
}