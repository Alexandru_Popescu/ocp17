package org.example.Chapter3MakingDecisions.ControllingFlowWithBranching.NestedLoop;

public class Labels {
    public static void main(String[] args) {

        int[][] a = {{1, 2, 3, 4}, {5, 6, 7, 8}, {9, 10, 11, 12}};

        Label1:
        for (int[] b : a) {
            Label2:
            for (int i = 0; i < b.length; i++) {
                System.out.print(b[i] + "\t");
            }
            System.out.println();

        }


    }
}
