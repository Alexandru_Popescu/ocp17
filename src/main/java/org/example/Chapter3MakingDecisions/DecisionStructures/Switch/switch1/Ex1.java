package org.example.Chapter3MakingDecisions.DecisionStructures.Switch.switch1;

import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int day = input.nextInt();
        if (day == 1) {
            System.out.println("Monday");

        } else if (day == 2) {
            System.out.println("Tuesday");

        } else if (day == 3) {
            System.out.println("Wednesday ");

        } else if (day == 4) {
            System.out.println("Thursday");

        } else if (day == 5) {
            System.out.println("Friday");

        }
        Scanner input2 = new Scanner(System.in);
        int day2 = input2.nextInt();
        switch (day2) {
            case 1:
                System.out.println("Monday");
                break;
            case 2:
                System.out.println("Tuesday");
                break;
            case 3:
                System.out.println("Wednesday ");
                break;
            case 4:
                System.out.println("Thursday");
                break;
            case 5:
                System.out.println("Friday");
                break;
        }

    }
}
