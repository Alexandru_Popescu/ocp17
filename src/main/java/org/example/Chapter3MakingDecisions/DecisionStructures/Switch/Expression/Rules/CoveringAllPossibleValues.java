package org.example.Chapter3MakingDecisions.DecisionStructures.Switch.Expression.Rules;

public class CoveringAllPossibleValues {


    enum Season {WINTER, SPRING, SUMMER, AUTUMN}


    String getWeather(Season value) {
        return switch (value) {
            case WINTER -> "Cold";
            case SPRING -> "Rainy";
            case SUMMER -> "Summer";
            case AUTUMN -> "Warm";
            default -> "Optional added ";

        };

    }
}