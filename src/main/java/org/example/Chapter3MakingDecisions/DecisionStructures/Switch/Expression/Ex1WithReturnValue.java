package org.example.Chapter3MakingDecisions.DecisionStructures.Switch.Expression;

public class Ex1WithReturnValue {
    public static void main(String[] args) {

        int day = 2;

        var check = switch(day){
            case 1 -> "Monday";
            case 2 -> "Tuesday";
            case 3 -> "Wednesday";
            case 4 -> "Thursday";
            case 5 -> "Friday";
            case 6 -> "Saturday";
            default -> "Wrong value";
        };
        System.out.println(check);

    }
}
