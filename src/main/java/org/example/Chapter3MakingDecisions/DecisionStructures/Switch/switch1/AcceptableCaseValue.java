package org.example.Chapter3MakingDecisions.DecisionStructures.Switch.switch1;

public class AcceptableCaseValue {


    final int getCookies() {
        return 4;
    }

    void feedAnimals() {
        final int bananas = 1;
        int apples = 2;
        int numberOfAnimals = 3;
        final int cookies = getCookies();

        switch (numberOfAnimals) {
            case bananas:   // market final and value is knows at compile-time
//            case apples:
//           case getCookies():
//            case cookies:
            case 3 * 5:


        }
    }
}
