package org.example.Chapter3MakingDecisions.DecisionStructures.Switch.Expression.Rules;

public class ReturningConsistentDataType {
    public static void main(String[] args) {

        int a = 10;

        int size = switch (a) {
            case 5 -> 1;
            case 10 -> (short) 2;
            default -> 5;
//            case 20 -> "3";  -> trb sa returneze un int
            // case 40 -> 4L;  -> Long > int - am nevoie de cast
            // case 50 -> null; -> nu se aplica la primitive

        };


    }

}
