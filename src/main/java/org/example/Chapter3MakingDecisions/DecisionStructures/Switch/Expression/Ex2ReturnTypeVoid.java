package org.example.Chapter3MakingDecisions.DecisionStructures.Switch.Expression;

public class Ex2ReturnTypeVoid {
    public static void main(String[] args) {

        Ex2ReturnTypeVoid ex2 = new Ex2ReturnTypeVoid();
        ex2.printSeason(3);


    }

    public void printSeason(int month) {
        switch (month) {
            case 1, 2, 3 -> System.out.println("Winter");
            case 4, 5, 6 -> System.out.println("Spring");
            case 7, 8 -> {
                System.out.println("Wrong");

            }

        }

    }
}
