package org.example.Chapter3MakingDecisions.DecisionStructures.Switch.Expression.Rules;

public class ApplyingCaseBlock {
    public static void main(String[] args) {

        int fish = 5;
        int length = 12;
        var name = switch (fish) {
            case 1 -> "Goldfish";
            case 2 -> {
                yield "Trout";
            }
            case 3 -> {
                if (length > 10) yield "Blobfish";
                else yield "Green";

            }
            default -> "Swordfish";

        };


    }

}
