package org.example.Chapter3MakingDecisions.DecisionStructures.Switch.switch1;

public class Main {
    public static void main(String[] args) {
        int a = 4;


        switch (a){
            case 1: case 2:
                System.out.println("ok");
            break;
            case 3, 4:
                System.out.println("good");
        }

//        switch (a){} -> works

        int month = 12;
        switch (month){
            case 1,2: case 3:
                System.out.println("A");
            case 4: case 5:
                System.out.println("B");
            default:
                System.out.println("NO");

        }

    }


}
