package org.example.Chapter3MakingDecisions.DecisionStructures.IfStatement.PatternMatching;
public class PatternMatchingWithInstanceOf {
    public static void main(String[] args) {
        var instance = new PatternMatchingWithInstanceOf();
        instance.instanceOf(4);

    }
    public void instanceOf(Number number) {
        if (number instanceof Integer) {
            Integer date = (Integer) number;
            System.out.println(date.compareTo(5));

        }
    }
    public void instanceOf2(Number number) {
        if (number instanceof final Integer  date) {
//            Integer date = (Integer) number;
//            date = 5; bad practice to reassign
            System.out.println(date.compareTo(5));
        }
    }
}