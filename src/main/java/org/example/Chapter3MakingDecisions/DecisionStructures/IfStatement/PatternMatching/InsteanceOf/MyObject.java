package org.example.Chapter3MakingDecisions.DecisionStructures.IfStatement.PatternMatching.InsteanceOf;


public class MyObject {
    public static void main(String[] args) {
//        System.out.println(null instanceof Object);  // false
        //System.out.println( null instanceof null);  // does not compile
 MyObject myObject = new MyObject();
 myObject.openZoo(3);

    }
    public void openZoo(Number time) {

        if (time instanceof Integer)
            System.out.println((Integer) time + "0' clock");
//        if (time instanceof String) --> atentie la type. A number cannot hold a String
        else
            System.out.println(time);

    }

    public void check(){
        openZoo(4);
    }
}
