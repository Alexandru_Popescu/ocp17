package org.example.Chapter3MakingDecisions.DecisionStructures.IfStatement;

import java.util.Scanner;

public class Ex2 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Please add a hour");
        int morning = scanner.nextInt();
        if (morning <= 4) {
            System.out.println("Good morning");
        } else if (morning <= 9)
            System.out.println("Weak up!");
        else {
            System.out.println("You are at work");
        }
    }
}