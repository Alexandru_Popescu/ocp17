package org.example.Chapter3MakingDecisions.DecisionStructures.IfStatement.PatternMatching;

public class PatternMatchingWithExpression {
    public static void main(String[] args) {
        var a = new PatternMatchingWithExpression();
        a.check(6);
        a.check(7);

    }


    public void check(Number number) {
        if (number instanceof final Integer data && data.compareTo(5) > 0) {
            System.out.println(data);
        } else {
            System.out.println("Wrong type");
        }
    }

// subtype

    /*

    Integer value =5;
    if (value instanceOf Integer time ) {} -> does n compile
     works when we mix classes with interfaces (Number and List)
     */


    // Flow scoping
//    public void checkTwo (Number number2){
//        if (number instanceof final Integer data || data.compareTo(5)>0){
////            System.out.println(data);
////        }
//        else {
//            System.out.println("Wrong type");
//        }
//    }


}