package org.example.Chapter3MakingDecisions.DecisionStructures.IfStatement;

public class Main {
    public static void main(String[] args) {

        int x = 1;
        if (x > 4)
            System.out.println("It's ok");
        x++;

//atentie la {}, la indentation
        System.out.println(x);


    }
}
